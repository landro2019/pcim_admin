  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link {{ request()->is('tableau-de-bord') || request()->is('statistics*') ? '' : 'collapsed' }}" href="{{ route('dashboard') }}">
          <i class="ri-layout-grid-fill"></i>
          <span>Tableau de bord</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link {{ request()->is('groupes') || request()->is('groupes/modifier-un-groupe/*') ? '' : 'collapsed' }}" href="{{ route('groupes') }}">
          <i class="bi bi-people-fill"></i>
          <span>Groupes</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link {{ request()->is('membres') || request()->is('membres/modifier-detail-un-membre/*') ? '' : 'collapsed' }}" href="{{ route('membres') }}">
          <i class="bi bi-person-square"></i>
          <span>Membres</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link {{ request()->is('cotisations') || request()->is('cotisations/modifier-une-cotisation/*') ? '' : 'collapsed' }}" href="{{ route('cotisations') }}">
          <i class="bi bi-wallet-fill"></i>
          <span>Cotisations</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link {{ request()->is('annonces') || request()->is('annonces/modifier-une-annonce/*') ? '' : 'collapsed' }}" href="{{ route('annonces') }}">
          <i class="bi bi-megaphone-fill"></i>
          <span>Annonces</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link {{ request()->is('notifications') || request()->is('notifications/modifier-une-notification/*') ? '' : 'collapsed' }}" href="{{ route('notifications') }}">
          <i class="bi bi-bell-fill"></i>
          <span>Notifications</span>
        </a>
      </li><!-- End Dashboard Nav -->
      
      <li class="nav-item">
        <a class="nav-link {{ 
                      request()->is('messes/categories') || 
                      request()->is('messes/categories/modifier-une-catégorie/*') || 
                      request()->is('messes/defauts') || 
                      request()->is('messes/defauts/modification/*') || 
                      request()->is('messes/demandes') || 
                      request()->is('messes/demandes/modification/*') ||
                      request()->is('messes/demandes/facture')
                      ? '' : 'collapsed' }}" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
           <i class="bi bi-house-fill"></i>
            <span>Demendes de messe</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tables-nav" class="nav-content collapse {{ 
                      request()->is('messes/categories') || 
                      request()->is('messes/categories/modifier-une-catégorie/*') || 
                      request()->is('messes/defauts') || 
                      request()->is('messes/defauts/modification/*') || 
                      request()->is('messes/demandes') || 
                      request()->is('messes/demandes/modification/*') ||
                      request()->is('messes/demandes/facture')
                      ? 'show' : '' }}" data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{ route('messes.categories') }}" class="{{ request()->is('messes/categories') || request()->is('messes/categories/modifier-une-catégorie/*') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Gatégories</span>
            </a>
          </li>
          <li>
            <a href="{{ route('messes.defauts') }}" class="{{ request()->is('messes/defauts') || request()->is('messes/defauts/modification/*') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Demande par defaut</span>
            </a>
          </li>
          <li>
            <a href="{{ route('messes.demandes') }}" class="{{ request()->is('messes/demandes') || request()->is('messes/demandes/modification/*') || request()->is('messes/demandes/facture') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Demandes</span>
            </a>
          </li>
        </ul>
      </li><!-- End Tables Nav -->

      <li class="nav-item">
        <a class="nav-link {{ request()->is('parametres') /* || request()->is('parametres/modifier-une-notification/*')  */ ? '' : 'collapsed' }}" href="{{ route('parametres') }}">
          <i class="bi bi-gear-fill"></i>
          {{-- <i class="bi bi-sliders"></i> --}}

          <span>Paramètre</span>
        </a>
      </li><!-- End Dashboard Nav -->

    </ul>

  </aside><!-- End Sidebar-->