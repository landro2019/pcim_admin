@extends('layout.master')

@section('title', 'PCIM | Groupes(Modifier une annonce)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('annonces.update', ['id' => $annonce->id ]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier une annnonce</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <label for="" style="font-size: 12px;">Titre</label>
                            <input name="titre" type="text" value="{{ $annonce->titre }}" class="form-control form-control-sm" placeholder="Titre">
                        </div>
                        <div class="">
                            <label for="" style="font-size: 12px;">Auteur</label>
                            <input name="auteur" type="text" value="{{ $annonce->auteur }}" class="form-control form-control-sm" placeholder="Auteur">
                        </div>
                        <div class="">
                            <label for="" style="font-size: 12px;">Message</label>
                            <textarea name="message" class="form-control form-control-sm" placeholder="Message">{{ $annonce->message }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('annonces') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection