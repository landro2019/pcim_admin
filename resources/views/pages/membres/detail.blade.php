@extends('layout.master')

@section('title', 'PCIM | Membres(Détail d\'un membre)')

@section('content')



    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="title-card fw-bold">Détail d'un membre</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="">
                                <table>
                                    <tbody>
                                        <tr><td class="fw-bold">Matricule</td><td>: {{ $membre->matricule }}</td></tr>
                                        <tr><td class="fw-bold">Nom & Prenom</td><td>: {{ $membre->firstname }} {{ $membre->lastname }}</td></tr>
                                        <tr><td class="fw-bold">Téléphone</td><td class="{{ $membre->phone == null ? 'text-danger':'' }}">: {{ $membre->phone ?? 'Pas de téléphone' }} </td></tr>
                                        <tr><td class="fw-bold">E-mail</td><td class="{{ $membre->email == null ? 'text-danger':'' }}">: {{ $membre->email ?? 'Pas d\'e-mail' }}</td></tr>
                                        <tr><td class="fw-bold">Adresse</td><td class="{{ $membre->address == null ? 'text-danger':'' }}">: {{ $membre->address ?? 'Pas d\'adresse' }}</td></tr>
                                        <tr><td class="fw-bold">Genre</td><td class="{{ $membre->sexe == null ? 'text-danger':'' }}">: {{ $membre->sexe ?? 'Pas de genre' }}</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <table>
                                    <tbody>
                                        <tr><td class="fw-bold">Quartier</td><td class="{{ $membre->quartier == null ? 'text-danger':'' }}">: {{ $membre->quartier ?? 'Pas de quartier' }}</td></tr>
                                        <tr><td class="fw-bold">Profession</td><td class="{{ $membre->profession == null ? 'text-danger':'' }}">: {{ $membre->profession ?? 'Pas de profession' }}</td></tr>
                                        <tr><td class="fw-bold">Région</td><td class="{{ $membre->region == null ? 'text-danger':'' }}">: {{ $membre->region ?? 'Pas de région' }}</td></tr>
                                        @isset($membre->group)
                                            <tr><td class="fw-bold">Groupe</td><td>: {{ $membre->group->label }}</td></tr>
                                        @endisset
                                        <tr><td class="fw-bold">Role</td><td>:
                                             @if ($membre->role == 'default')
                                                <span class="text-info fw-bold">Membre</span>
                                            @endif
                                            @if ($membre->role == 'admin')
                                                <span class="text-success fw-bold">Administrateur</span>
                                            @endif</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="p-3 alert alert-warning" id="cashAlert" style="display: none;">
        <div class="d-flex align-items-center">
            Voulez-vous vraiment effectuer cet paiement par cash ?
           <div class=""> 
                <form id="cashForm" class="p-0 m-0" method="post">
                    @method('PUT')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                </form>
           </div>
            <button class="btn btn-sm btn-primary ms-2" onclick="closeAlertC()">NON</button>
        </div>
    </div>

    <div class="p-3 alert alert-warning" id="moneyAlert" style="display: none;">
        <div class="d-flex align-items-center">
            Voulez-vous vraiment effectuer cet paiement par mobile money ?
            <button class="btn btn-sm btn-danger ms-3"id='FormConfirm'>OUI</button>
            <button class="btn btn-sm btn-primary ms-2" onclick="closeAlertM()">NON</button>
        </div>
    </div>



    <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
        <div class="d-flex align-items-center">
            Voulez-vous vraiment supprimer cette cotisation ?
            <div class="">
                <form id="deleteForm" class="p-0 m-0" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                </form>
            </div>
            <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
        </div>
    </div>

    <script>
        function confirmDelete(itemId) {

        var route = "{{ route('cotisations.delete.user', ['id' => ':itemId']) }}";
        route = route.replace(':itemId', itemId);

            document.getElementById('deleteForm').action = route //"cotisations/supprimer-une-cotisation-membre/" + itemId; // Remplacez "URL_DU_FORMULAIRE" par l'URL réelle du formulaire en ajoutant l'ID à la fin
            document.getElementById('confirmationAlert').style.display = 'block';
        }

        function closeAlert() {
            document.getElementById('confirmationAlert').style.display = 'none';
        }
    </script>





    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="title-card fw-bold">Liste des cotisations</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th scope="col">Type</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Montant</th>
                                                <th scope="col">Echéance</th>
                                                <th scope="col">Création</th>
                                                <th scope="col">Groupe</th>
                                                <th scope="col">Paiement</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if (isset($cotisations) && $cotisations !== null && $cotisations !== [])
                                            @foreach ($cotisations as $item)
                                            <tr>
                                                <td>{{ $item->cotisation->type }}</td>
                                                <td>{{ $item->cotisation->description }}</td>
                                                <td>{{ $item->cotisation->montant }}</td>
                                                <td>{{ Carbon\Carbon::parse($item->cotisation->date_start)->format('d-m-Y')}}</td>
                                                <td>{{ Carbon\Carbon::parse($item->createdAt)->format('d-m-Y')}}</td>
                                                <td>
                                                    @if ($item->cotisation->group != null)
                                                        {{ $item->cotisation->group->label }}
                                                    @endif
                                                </td> 
                                                <td>
                                                    <div class="dropdown">
                                                    <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        <i class="ri-more-2-fill"></i>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item text-primary fw-bold" onclick="cash({{ $item->id }})" href="#">Cash</a></li>
                                                        <li><a class="dropdown-item text-info fw-bold"onclick="money({{ $item->id }})" href="#">Mobile Money</a></li>
                                                        <li>
                                                            <a class="dropdown-item text-danger fw-bold" href="#" onclick="confirmDelete({{ $item->id }})">Supprimer</a>
                                                        </li>
                                                    </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach 
                                        @else
                                            <tr>
                                            <td colspan="6">
                                                <div class="d-flex justify-content-center">
                                                <div class="spinner-border me-3" role="status"></div> Aucune cotisation disponible
                                                </div>
                                            </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>








    <!-- Basic Modal -->
  <div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('membres.payeM') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Effecter un paiement mobil money</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="">
                        <input type="hidden" name="idCotisation" id="monId">
                        <input name="phone" type="text" class="form-control form-control-sm" placeholder="Numéro de téléphone">
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Valider</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->



      <!-- Basic Modal -->
      <div class="modal fade" id="ConfirmModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
                <div class="modal-header py-2">
                    <h5 class="modal-title">Confirmation du paiement mobil money</h5>
                    {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
                </div>
                <div class="modal-body">
                    <div class="px-5">
                        <a id="lien" class="btn btn-sm btn-success w-100" target="blank">Confirmer le paiement</a>
                    </div>
                </div>
                <div class="modal-footer py-1">
                    <button id="fermerModal" class="btn btn-sm btn-primary">Valider</button>
                    {{-- <button type="button" class="btn btn-secondary btn-sm"  data-bs-dismiss="modal">Fermer</button> --}}
                </div>
          </div>
        </div>
      </div><!-- End Basic Modal-->
@endsection


<script>
    function cash(itemId) {
        var route = "{{ route('membres.paye', ['id' => ':itemId', 'key' => 'cash']) }}";
        route = route.replace(':itemId', itemId);

        var cashForm = document.getElementById('cashForm');
        cashForm.action = route;

        var cashAlert = document.getElementById('cashAlert');
        cashAlert.style.display = 'block';
    }

    function closeAlertC() {
        var cashAlert = document.getElementById('cashAlert');
        cashAlert.style.display = 'none';
    }
</script>



<script>
    // Fonction pour stocker la dernière valeur passée en paramètre dans le navigateur (localStorage)
    function setLastItemId(itemId) {
        localStorage.setItem('lastItemId', itemId);
    }

    // Fonction pour récupérer la dernière valeur stockée dans le navigateur (localStorage)
    function getLastItemId() {
        return localStorage.getItem('lastItemId');
    }


    function money(itemId) {
        document.getElementById('moneyAlert').style.display = 'block';

        var FormConfirm = document.getElementById('FormConfirm');
        var monId = document.getElementById('monId');

        FormConfirm.addEventListener('click', function() {
            document.getElementById('moneyAlert').style.display = 'none';
            // Ici, nous sélectionnons le modal par son ID "basicModal" et nous le récupérons en utilisant le Bootstrap Modal API.
            var modalElement = document.getElementById('basicModal');
            // Ensuite, nous créons une instance du modal en utilisant la classe Modal de Bootstrap.
            var modalInstance = new bootstrap.Modal(modalElement);
            // Enfin, nous ouvrons le modal en appelant la méthode "show()" sur l'instance du modal.
            modalInstance.show();

            monId.value = itemId;
        });
        setLastItemId(itemId); // Stocker la dernière valeur passée en paramètre dans le navigateur
    }

    function closeAlertM() {
        document.getElementById('moneyAlert').style.display = 'none';
    }

    // Fonction pour fermer le modal et soumettre le formulaire
    function fermerModalEtSoumettre() {
        var lastItemId = getLastItemId(); // Récupérer la dernière valeur stockée dans le navigateur
        var route = "{{ route('membres.paye_status', ['id' => ':itemId']) }}";
        route = route.replace(':itemId', lastItemId);

        // Créer un formulaire de type GET de manière dynamique
        var form = document.createElement('form');
        form.action = route;
        form.method = 'GET';

        // Ajouter le formulaire à la page et le soumettre automatiquement
        document.body.appendChild(form);
        form.submit();
    }

</script>

<script>
    // JavaScript
  
    // Événement de chargement de page (se déclenche après le rafraîchissement complet de la page)
    window.addEventListener('load', function() {
      // Vérifier si la page a reçu l'objet "payment" en tant que donnée de session
      var paymentData = @json(session('payment')); // Convertir les données PHP en objet JavaScript
  
      // Vérifier si l'objet "paymentData" existe et n'est pas vide
      if (paymentData && Object.keys(paymentData).length > 0) {
        // Ici, nous sélectionnons le modal par son ID "ConfirmModal" et nous le récupérons en utilisant le Bootstrap Modal API.
        var modalElement = document.getElementById('ConfirmModal');
  
        // Ensuite, nous créons une instance du modal en utilisant la classe Modal de Bootstrap.
        var modalInstance = new bootstrap.Modal(modalElement);
  
        // Enfin, nous ouvrons le modal en appelant la méthode "show()" sur l'instance du modal.
        modalInstance.show();

        var lien = document.getElementById('lien');
        lien.href = paymentData.payment_url ;

        var validerBtn = document.getElementById("fermerModal");

        validerBtn.addEventListener("click", function() {
            modalInstance.hide(); // Fermer le modal après le clic sur "Valider"
            console.log("Clic sur le bouton Valider");
        });

        // des que le modal se ferme , appel du funtion fermerModalEtSoumettre()
        modalInstance._element.addEventListener("hidden.bs.modal", function() {
        // Appeler votre autre fonction ici
            console.log("Le modal est maintenant fermé, on peut appeler une autre fonction !");
            fermerModalEtSoumettre();
        });

      }
    });
</script>