@extends('layout.master')

@section('title', 'PCIM | Membres(Modifier un membre)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('membres.update', ['id' => $membre->id ]) }}" method="post">
                @method('PATCH')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier un membre</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <div class="mb-2">
                                <label for="" style="font-size: 12px;">Groupe</label>
                                <select name="groupe" class="form-select form-select-sm mt-2">
                                    @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                                        <option selected hidden disabled>-- Selectionner un groupe --</option>
                                        @foreach ($groupes as $item)
                                            <option
                                                value="{{ $item->id }}" 
                                                @isset($membre->group)
                                                  {{ $membre->group->id == $item->id ? 'selected' : '' }}
                                                @endisset
                                            >
                                              {{ $item->label }}
                                            </option>
                                        @endforeach
                                    @else
                                        <option selected hidden disabled>-- Aucun groupe disponible --</option>
                                    @endif
                                </select>
                            </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Nom</label>
                                <input type="text" value="{{ $membre->firstname }}" name="firstname" class="form-control form-control-sm" placeholder="Nom">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Prenom</label>
                                <input type="text" value="{{ $membre->lastname }}" name="lastname" class="form-control form-control-sm" placeholder="Prenom">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Téléphone</label>
                                <input type="text" value="{{ $membre->phone }}" name="phone" class="form-control form-control-sm" placeholder="Téléphone">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Profession</label>
                                <input type="text" value="{{ $membre->profession }}" name="profession" class="form-control form-control-sm" placeholder="Profession">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">E-mail</label>
                                <input type="email" value="{{ $membre->email }}" name="email" class="form-control form-control-sm" placeholder="E-mail">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Région</label>
                                <input type="text" value="{{ $membre->region }}" name="region" class="form-control form-control-sm" placeholder="Région">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Address</label>
                                <input type="text" value="{{ $membre->address }}" name="address" class="form-control form-control-sm" placeholder="Address">
                              </div>
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Quartier</label>
                                <input type="text" value="{{ $membre->quartier }}" name="quartier" class="form-control form-control-sm" placeholder="Quartier">
                              </div>
{{--                               
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Mot de passe</label>
                                <div class="password-container">
                                  <input name="password" type="password" id="password-field" class="form-control form-control-sm" placeholder="Mot de passe"> 
                                  <span class="password-toggle" id="password-toggle">&#x1F441;</span>
                                  
                                  <style>
                                    .password-container {
                                      position: relative;
                                    }
                                    
                                    .password-toggle {
                                      position: absolute;
                                      top: 50%;
                                      right: 10px;
                                      transform: translateY(-50%);
                                      cursor: pointer;
                                    }
                                  </style>
                                  
                                  <script>
                                    document.addEventListener('DOMContentLoaded', function() {
                                      const passwordField = document.getElementById('password-field');
                                      const passwordToggle = document.getElementById('password-toggle');
                                  
                                      passwordToggle.addEventListener('click', function() {
                                        if (passwordField.type === 'password') {
                                          passwordField.type = 'text';
                                          passwordToggle.innerHTML = '&#x1F576;';
                                        } else {
                                          passwordField.type = 'password';
                                          passwordToggle.innerHTML = '&#x1F441;';
                                        }
                                      });
                                    });
                                  </script>
                                  
                                </div>
                              </div>
 --}}
                              <div class="mb-2">
                                <label for="" style="font-size: 12px;">Genre</label>
                                <div class="d-flex">
                                  <div class="me-3">
                                    <input type="radio" name="sexe" value="Masculin" {{ $membre->sexe == 'Masculin' ? 'checked' : '' }} >
                                    <label for="">Masculin</label>
                                  </div>
                                  <div class="">
                                    <input type="radio" name="sexe" value="Féminin" {{ $membre->sexe == 'Féminin' ? 'checked' : '' }}>
                                    <label for="">Féminin</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('membres') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection