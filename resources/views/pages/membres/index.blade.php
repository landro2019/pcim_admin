@extends('layout.master')

@section('title', 'PCIM | Membres')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-12">

        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title">liste des membres</h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Créer un membre
                    </button>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <table class="table datatable">
              <thead>
                  <tr>
                      <th scope="col">Matricule</th>
                      <th scope="col">Nom & Prenom</th>
                      <th scope="col">Téléphone</th>
                      <th scope="col">Genre</th>
                      <th scope="col">Date</th>
                      <th scope="col">Role</th>
                      <th scope="col">Actions</th>
                  </tr>
              </thead>
              <tbody>
                @if (isset($membres) && $membres !== null && $membres !== [])
                  @foreach ($membres as $item)
                    <tr>
                        <td>{{ $item->matricule }} </td>
                        <td>{{ $item->firstname }} {{ $item->lastname }} </td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ $item->sexe }}</td>
                        <td>{{ Carbon\Carbon::parse($item->createdAt)->format('d-m-Y')}}</td>
                        <td>
                          @if ($item->role == 'default')
                            <span class="text-info fw-bold">Membre</span>
                          @endif
                          @if ($item->role == 'admin')
                            <span class="text-success fw-bold">Administrateur</span>
                          @endif
                        </td> 
                        <td>
                          <div class="dropdown">
                            <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="ri-more-2-fill"></i>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item text-primary fw-bold" href="{{ route('membres.get_byId', ['id' => $item->id, 'key' => 'detail' ]) }}">Détail</a></li>
                                <li><a class="dropdown-item text-info fw-bold" href="{{ route('membres.get_byId', ['id' => $item->id, 'key' => 'modifier' ]) }}">Modifier</a></li>
                                <form action="{{ route('membres.role', ['id' => $item->id ]) }}" method="post">
                                  @csrf
                                  @method('PUT')
                                  <li><button class="dropdown-item text-warning fw-bold" type="submit">{{ $item->role == 'admin' ? 'Désactiver administrateur' : 'Devenir administrateur' }}</button></li>
                                </form>
                                <li>
                                  <form action="{{ route('membres.reset', ['id' => $item->id ]) }}" method="post">
                                    @method('PUT')
                                    @csrf
                                    <button class="dropdown-item text-success fw-bold">Réinitialiser mot de passe</button>
                                  </form>
                                </li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                  @endforeach 
                @else
                  <tr>
                    <td colspan="7">
                      <div class="d-flex justify-content-center">
                        <div class="spinner-border me-3" role="status"></div> Aucun membre disponible
                      </div>
                    </td>
                  </tr>
                @endif
              </tbody>
            </table>
            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>

















<!-- Basic Modal -->
  <div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('membres.create') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Créer un nouveau membre</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="mb-2">
                      <select name="groupe" class="form-select form-select-sm mt-2">
                          @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                              <option selected hidden disabled>-- Selectionner un groupe --</option>
                              @foreach ($groupes as $item)
                                  <option value="{{ $item->id }}">{{ $item->label }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucun groupe disponible --</option>
                          @endif
                      </select>
                    </div>
                    <div class="mb-2">
                        <input type="text" name="firstname" class="form-control form-control-sm" placeholder="Nom">
                    </div>
                    <div class="mb-2">
                      <input type="text" name="lastname" class="form-control form-control-sm" placeholder="Prenom">
                    </div>
                    <div class="mb-2">
                      <input type="text" name="phone" class="form-control form-control-sm" placeholder="Téléphone">
                    </div>
                    <div class="mb-2">
                      <input type="text" name="profession" class="form-control form-control-sm" placeholder="Profession">
                    </div>
                    <div class="mb-2">
                      <input type="email" name="email" class="form-control form-control-sm" placeholder="E-mail">
                    </div>
                    <div class="mb-2">
                      <input type="text" name="region" class="form-control form-control-sm" placeholder="Région">
                    </div>
                    <div class="mb-2">
                      <input type="text" name="address" class="form-control form-control-sm" placeholder="address">
                    </div>
                    <div class="mb-2">
                      <input type="text" name="quartier" class="form-control form-control-sm" placeholder="Quartier">
                    </div>
                    <div class="mb-2">
                      <div class="password-container">
                        <input name="password" type="password" id="password-field" class="form-control form-control-sm" placeholder="Mot de passe"> 
                        <span class="password-toggle" id="password-toggle">&#x1F441;</span>
                        
                        <style>
                          .password-container {
                            position: relative;
                          }
                          
                          .password-toggle {
                            position: absolute;
                            top: 50%;
                            right: 10px;
                            transform: translateY(-50%);
                            cursor: pointer;
                          }
                        </style>
                        
                        <script>
                          document.addEventListener('DOMContentLoaded', function() {
                            const passwordField = document.getElementById('password-field');
                            const passwordToggle = document.getElementById('password-toggle');
                        
                            passwordToggle.addEventListener('click', function() {
                              if (passwordField.type === 'password') {
                                passwordField.type = 'text';
                                passwordToggle.innerHTML = '&#x1F576;';
                              } else {
                                passwordField.type = 'password';
                                passwordToggle.innerHTML = '&#x1F441;';
                              }
                            });
                          });
                        </script>
                        
                      </div>
                    </div>
                    <div class="mb-2">
                      <label for="" style="font-size: 12px;">Genre</label>
                      <div class="d-flex">
                        <div class="me-3">
                          <input type="radio" name="sexe" value="Masculin" checked>
                          <label for="">Masculin</label>
                        </div>
                        <div class="">
                          <input type="radio" name="sexe" value="Féminin">
                          <label for="">Féminin</label>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->

@endsection