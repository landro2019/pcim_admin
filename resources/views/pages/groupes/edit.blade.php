@extends('layout.master')

@section('title', 'PCIM | Groupes(Modifier un groupe)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('groupes.update', ['id' => $groupe->id ]) }}" method="post">
                @method('PATCH')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier un groupe</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <label for="" style="font-size: 12px;">Groupe</label>
                            <input name="label" value="{{ $groupe->label }}" type="text" class="form-control form-control-sm" placeholder="Groupe">
                        </div>
                        <div class="">
                            <label for="" style="font-size: 12px;">Description</label>
                            <textarea name="description" class="form-control form-control-sm" placeholder="Description">{{ $groupe->description }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('groupes') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection