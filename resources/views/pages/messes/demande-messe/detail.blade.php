@extends('layout.master')

@section('title', 'PCIM | Messes(Détail d\'une demande de messe)')

@section('content')



    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="title-card fw-bold">Détail d'une demande de messe</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <table>
                                  <tbody>
                                        <tr><td class="fw-bold">Demandeur</td><td>: {{ $demande->user->firstname }} {{ $demande->user->lastname }}</td></tr>
                                        <tr><td class="fw-bold">Catégorie</td><td>: {{ $demande->categorie->nom }}</td></tr>
                                        <tr><td class="fw-bold">Reference</td><td class="{{ $demande->reference == null ? 'text-danger':'' }}">: {{ $demande->reference ?? 'Pas de référence' }} </td></tr>
                                        <tr><td class="fw-bold">Moyen de paiement</td><td class="{{ $demande->moyenPayment == null ? 'text-danger':'' }}">: {{ $demande->moyenPayment ?? 'Aucun paiement effectué' }} </td></tr>
                                        <tr><td class="fw-bold">Numéro de téléphone de paiement</td><td class="{{ $demande->phone == null ? 'text-danger':'' }}">: {{ $demande->phone ?? 'Pas de numéro' }}</td></tr>
                                        <tr><td class="fw-bold">Date de paiement</td><td class="{{ $demande->datePayment == null ? 'text-danger':'' }}">: {{ $demande->datePayment ?? 'Pas de date' }}</td></tr>
                                        <tr><td class="fw-bold">Montant</td><td class="{{ $demande->montant == null ? 'text-danger':'' }}">: {{ $demande->montant ?? 'Pas de montant' }}</td></tr>
                                        <tr><td class="fw-bold">Paiement</td><td class="{{ $demande->etat == 'IMPAYE' ? 'text-danger' : 'text-success' }}">: {{ $demande->etat }}</td></tr>
                                        <tr><td class="fw-bold">Célébré</td><td class="{{ $demande->celebre == 'NON' ? 'text-danger' : 'text-success' }}">: {{ $demande->celebre }}</td></tr>
                                        <tr><td class="fw-bold">Date de demande de messe</td><td class="{{ $demande->dateDebut == null ? 'text-danger':'' }}">: {{ $demande->dateDebut ?? 'Pas de date' }}</td></tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12 mt-5">
                            <div class="">
                                <div class="fw-bold">Message :</div>
                                <div class="text-justify">{{ $demande->message }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
