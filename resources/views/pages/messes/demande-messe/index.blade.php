@extends('layout.master')

@section('title', 'PCIM | Demandes de messe')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
        <div class="d-flex align-items-center">
            Voulez-vous vraiment supprimer cette demande ?
            <div class="">
                <form id="deleteForm" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                </form>
            </div>
            <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
        </div>
    </div>
    <script>
        function confirmDelete(itemId) {
            var route = "{{ route('messes.demandes.delete', ['id' => ':itemId']) }}";
            route = route.replace(':itemId', itemId);
            document.getElementById('deleteForm').action = route; 
            document.getElementById('confirmationAlert').style.display = 'block';
        }

        function closeAlert() {
            document.getElementById('confirmationAlert').style.display = 'none';
        }
    </script>



    <div class="p-3 alert alert-warning" id="cashAlert" style="display: none;">
      <div class="d-flex align-items-center">
          Voulez-vous vraiment effectuer cet paiement par cash ?
        <div class=""> 
              <form id="cashForm" class="p-0 m-0" method="post">
                  @method('PUT')
                  @csrf
                  <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
              </form>
        </div>
          <button class="btn btn-sm btn-primary ms-2" onclick="closeAlertC()">NON</button>
      </div>
    </div>
    <script>
      function cash(itemId) {
          var route = "{{ route('messes.demandes.paye', ['id' => ':itemId']) }}";
          route = route.replace(':itemId', itemId);
  
          var cashForm = document.getElementById('cashForm');
          cashForm.action = route;
  
          var cashAlert = document.getElementById('cashAlert');
          cashAlert.style.display = 'block';
      }
  
      function closeAlertC() {
          var cashAlert = document.getElementById('cashAlert');
          cashAlert.style.display = 'none';
      }
    </script>





    <div class="p-3 alert alert-warning" id="moneyAlert" style="display: none;">
      <div class="d-flex align-items-center">
          Voulez-vous vraiment effectuer cet paiement par mobile money ?
          <button class="btn btn-sm btn-danger ms-3"id='FormConfirm'>OUI</button>
          <button class="btn btn-sm btn-primary ms-2" onclick="closeAlertM()">NON</button>
      </div>
    </div>


        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title">liste des demandes de messe</h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;"  data-bs-toggle="modal" data-bs-target="#imprimerModal">Imprimer</button>
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModalCreer">
                        Créer une demande de messe
                    </button>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <div class="table-responsive">
              <table class="table datatable">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Membre</th>
                        <th scope="col">Catégorie</th>
                        <th scope="col">Montant</th>
                        <th scope="col"><nobr>Date de demande de messe</nobr></th>
                        <th scope="col">Celebré</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                  @if (isset($demamdes) && $demamdes !== null && $demamdes !== [])
                    @foreach ($demamdes as $item)
                      <tr>
                          <td>#{{ $item->id }}</td>
                          <td><nobr>{{ $item->user->firstname }} {{ $item->user->lastname }}</nobr> </td>
                          <td><nobr>{{ $item->categorie->nom }} </nobr></td>
                          <td><nobr>{{ number_format($item->montant, 0, ',', ' ') }} F CFA</nobr></td>
                          <td><nobr>{{ Carbon\Carbon::parse($item->dateDebut)->format('d-m-Y')}}</nobr></td>
                          <td><span class="{{ $item->celebre == 'NON' ? 'text-danger' : 'text-success' }}">{{ $item->celebre }}</span></td>
                          <td><span class="{{ $item->etat == 'IMPAYE' ? 'text-danger' : 'text-success' }}">{{ $item->etat }}</span></td>
                          <td>
                            <div class="dropdown">
                              <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                  <i class="ri-more-2-fill"></i>
                              </button>
                              <ul class="dropdown-menu">
                                <li>
                                  <form class="m-0" action="{{ route('messes.demandes.celebre', ['id' => $item->id ]) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="dropdown-item text-dark fw-bold" >Célébré</button>
                                  </form>
                                </li>
                                <li><a class="dropdown-item text-secondary fw-bold" href="{{ route('messes.demandes.detail', ['id' => $item->id ]) }}">Détail</a></li>
                                @if ($item->etat == 'IMPAYE')
                                  <li><a class="dropdown-item text-primary fw-bold" onclick="cash({{ $item->id }})" href="#">Cash</a></li>
                                  <li><a class="dropdown-item text-warning fw-bold"onclick="money({{ $item->id }})" href="#">Mobile Money</a></li>
                                  <li><a class="dropdown-item text-info fw-bold" href="{{ route('messes.demandes.get_byId', ['id' => $item->id ]) }}">Modifier</a></li>
                                 
                                  <li><a class="dropdown-item text-danger fw-bold" href="#" onclick="confirmDelete({{ $item->id }})">Supprimer</a></li>
                                @endif

                              </ul>
                            </div>
                          </td>
                       
                      </tr>
                    @endforeach 
                  @else
                    <tr>
                      <td colspan="9">
                        <div class="d-flex justify-content-center">
                          <div class="spinner-border me-3" role="status"></div> Aucune demande disponible
                        </div>
                      </td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>

















<!-- Basic Modal -->
  <div class="modal fade" id="basicModalCreer" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalCreerLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('messes.demandes.create') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Créer une nouvelle demande de messe</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="mb-2">
                      <select name="user" class="form-select form-select-sm mt-2">
                          @if (isset($membres) && $membres !== null  && $membres !== [])
                              <option selected hidden disabled>-- Selectionner un membre --</option>
                              @foreach ($membres as $item)
                                  <option value="{{ $item->id }}">{{ $item->firstname }} {{ $item->lastname }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucun membre disponible --</option>
                          @endif
                      </select>
                    </div>
                    <div class="mb-2">
                      <select name="categorie" class="form-select form-select-sm mt-2">
                          @if (isset($categories) && $categories !== null  && $categories !== [])
                              <option selected hidden disabled>-- Selectionner une categorie --</option>
                              @foreach ($categories as $item)
                                  <option value="{{ $item->id }}">{{ $item->nom }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucune categorie disponible --</option>
                          @endif
                      </select>
                    </div>
                    <div class="mb-2">
                      <textarea name="message" class="form-control form-control-sm" rows="5" placeholder="Message"></textarea>
                    </div>
                    <div class="mb-2">
                      <label for="" style="font-size: 12px;">Date de demande de messe</label>
                      <input type="date" name="dateDebut" class="form-control form-control-sm" >
                    </div>
                    {{-- <div class="mb-2">
                      <label for="" style="font-size: 12px;">Date fin</label>
                      <input type="date" name="dateFin" class="form-control form-control-sm">
                    </div> --}}
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->

@endsection


    <!-- Basic Modal -->
    <div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="{{ route('messes.demandes.payeM') }}" method="post">
            @csrf
              <div class="modal-header py-2">
                  <h5 class="modal-title">Effecter un paiement mobil money</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                  <div class="">
                      <div class="">
                          <input type="hidden" name="idCotisation" id="monId">
                          <input name="phone" type="text" class="form-control form-control-sm" placeholder="Numéro de téléphone">
                      </div>
                  </div>
              </div>
              <div class="modal-footer py-1">
                  <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                  <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                  <button type="submit" class="btn btn-sm btn-primary">Valider</button>
              </div>
          </form>
        </div>
      </div>
    </div><!-- End Basic Modal-->
  
  
  
        <!-- Basic Modal -->
        <div class="modal fade" id="ConfirmModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
                  <div class="modal-header py-2">
                      <h5 class="modal-title">Confirmation du paiement mobil money</h5>
                      {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
                  </div>
                  <div class="modal-body">
                      <div class="px-5">
                          <a id="lien" class="btn btn-sm btn-success w-100" target="blank">Confirmer le paiement</a>
                      </div>
                  </div>
                  <div class="modal-footer py-1">
                      <button id="fermerModal" class="btn btn-sm btn-primary">Valider</button>
                      {{-- <button type="button" class="btn btn-secondary btn-sm"  data-bs-dismiss="modal">Fermer</button> --}}
                  </div>
            </div>
          </div>
        </div><!-- End Basic Modal-->






<!-- Modal de base -->
<div class="modal fade" id="imprimerModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header py-2">
        <h5 class="modal-title">Impression des demandes de messe</h5>
      </div>
      <div class="modal-body">
        <div class="px-5">
          <form class="m-0 p-0" id="formulaireImprimer">
            @csrf
            <input type="date" name="date" class="form-control form-control-sm">
          </form>
        </div>
      </div>
      <div class="modal-footer py-1">
        <button id="validerImprimerModal" class="btn btn-sm btn-primary">Valider</button>
      </div>
    </div>
  </div>
</div><!-- Fin du Modal de base -->

<script>
  // 'ai enveloppé le code JavaScript dans un événement "DOMContentLoaded" pour m'assurer que le code JavaScript ne s'exécute qu'une fois que l'ensemble du document HTML est chargé.
  document.addEventListener('DOMContentLoaded', function() {
    var imprimerModal = new bootstrap.Modal(document.getElementById('imprimerModal'));
    var validerImprimerModal = document.getElementById("validerImprimerModal");
    var formulaire = document.getElementById('formulaireImprimer');

    validerImprimerModal.addEventListener('click', function() {
      console.log('Validation en cours...');
      imprimerModal.hide(); // Fermer le modal après avoir cliqué sur "Valider"
    });

    imprimerModal._element.addEventListener("hidden.bs.modal", function() {
      console.log("Le modal est maintenant fermé, soumission du formulaire en cours...");
     // Ajouter un délai d'une seconde (1000 millisecondes) avant de soumettre le formulaire
     setTimeout(function() {
        formulaire.action = "{{ route('messes.demandes.facture') }}";
        formulaire.method = 'POST';
        formulaire.submit();
      }, 1000);
    });
  });
</script>






        <script>
          // Fonction pour stocker la dernière valeur passée en paramètre dans le navigateur (localStorage)
          function setLastItemId(itemId) {
              localStorage.setItem('lastItemId', itemId);
          }
      
          // Fonction pour récupérer la dernière valeur stockée dans le navigateur (localStorage)
          function getLastItemId() {
              return localStorage.getItem('lastItemId');
          }
      
      
          function money(itemId) {
              document.getElementById('moneyAlert').style.display = 'block';
      
              var FormConfirm = document.getElementById('FormConfirm');
              var monId = document.getElementById('monId');
      
              FormConfirm.addEventListener('click', function() {
                  document.getElementById('moneyAlert').style.display = 'none';
                  // Ici, nous sélectionnons le modal par son ID "basicModal" et nous le récupérons en utilisant le Bootstrap Modal API.
                  var modalElement = document.getElementById('basicModal');
                  // Ensuite, nous créons une instance du modal en utilisant la classe Modal de Bootstrap.
                  var modalInstance = new bootstrap.Modal(modalElement);
                  // Enfin, nous ouvrons le modal en appelant la méthode "show()" sur l'instance du modal.
                  modalInstance.show();
      
                  monId.value = itemId;
              });
              setLastItemId(itemId); // Stocker la dernière valeur passée en paramètre dans le navigateur
          }
      
          function closeAlertM() {
              document.getElementById('moneyAlert').style.display = 'none';
          }
      
          // Fonction pour fermer le modal et soumettre le formulaire
          function fermerModalEtSoumettre() {
              var lastItemId = getLastItemId(); // Récupérer la dernière valeur stockée dans le navigateur
              var route = "{{ route('messes.demandes.paye_status', ['id' => ':itemId']) }}";
              route = route.replace(':itemId', lastItemId);
      
              // Créer un formulaire de type GET de manière dynamique
              var form = document.createElement('form');
              form.action = route;
              form.method = 'GET';
      
              // Ajouter le formulaire à la page et le soumettre automatiquement
              document.body.appendChild(form);
              form.submit();
          }
      
      </script>
      
      <script>
          // JavaScript
        
          // Événement de chargement de page (se déclenche après le rafraîchissement complet de la page)
          window.addEventListener('load', function() {
            // Vérifier si la page a reçu l'objet "payment" en tant que donnée de session
            var paymentData = @json(session('payment')); // Convertir les données PHP en objet JavaScript
        
            // Vérifier si l'objet "paymentData" existe et n'est pas vide
            if (paymentData && Object.keys(paymentData).length > 0) {
              // Ici, nous sélectionnons le modal par son ID "ConfirmModal" et nous le récupérons en utilisant le Bootstrap Modal API.
              var modalElement = document.getElementById('ConfirmModal');
        
              // Ensuite, nous créons une instance du modal en utilisant la classe Modal de Bootstrap.
              var modalInstance = new bootstrap.Modal(modalElement);
        
              // Enfin, nous ouvrons le modal en appelant la méthode "show()" sur l'instance du modal.
              modalInstance.show();
      
              var lien = document.getElementById('lien');
              lien.href = paymentData.payment_url ;
      
              var validerBtn = document.getElementById("fermerModal");
      
              validerBtn.addEventListener("click", function() {
                  modalInstance.hide(); // Fermer le modal après le clic sur "Valider"
                  console.log("Clic sur le bouton Valider");
              });
      
              // des que le modal se ferme , appel du funtion fermerModalEtSoumettre()
              modalInstance._element.addEventListener("hidden.bs.modal", function() {
              // Appeler votre autre fonction ici
                  console.log("Le modal est maintenant fermé, on peut appeler une autre fonction !");
                  fermerModalEtSoumettre();
              });
      
            }
          });
      </script>