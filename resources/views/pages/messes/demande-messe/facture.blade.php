@extends('layout.master')

@section('title', 'PCIM | Facture')

@section('content')

<style>
  @page {
      size: A5; 
      margin: 0;
  }

  .page { 
     /*  width: 21cm;  */
      /* min-height: 29.7cm;  */
      /*margin: 1cm auto;  
      /* border: 1px #D3D3D3 solid; */
      /* border-radius: 5px; */
      background: white;
      /* box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); */
  } 
</style> 

    <div class="row">
        
        <div class="col-md-8">
          <div class="p-3" style="background-color: white;">
            <div class="" id="invoice">
              <div class="page" >
                <h2>Demandes de messe</h2><hr><br>
          
                @foreach ($groupedData as $key => $items)
                  <h3>Demandeur</h3>
                  <p><strong>Nom:</strong> {{ preg_replace("/\[\d+\]/", "", $key) }}</p>
              
                  <h4>Intentions</h4>
                  
                    @foreach ($items as $index)
                      <table border="0">
                        <p><strong>#{{ $index->id  }}</strong></p>
                        <thead>
                          <tr style="text-align: left;">
                            <th>Date: {{ Carbon\Carbon::parse($index->dateDebut)->format('d-m-Y') }}</th>
                            {{-- <th>Date début: {{ Carbon\Carbon::parse($index->dateDebut)->format('d-m-Y') }}</th>
                            <th>Date fin: {{ Carbon\Carbon::parse($index->dateFin)->format('d-m-Y') }}</th> --}}
                          </tr>
                          <tr style="text-align: left;">
                            @php  
                                $date1 = new DateTime($index->dateDebut);
                                $date2 = new DateTime($index->dateFin);

                                $interval = $date1->diff($date2);
                                $numberOfDays = $interval->days;
                            @endphp
                            {{-- <th colspan="2">Nombre de jour: {{ $numberOfDays }}</th> --}}
                            
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td colspan="2">
                              <p>{{$index->message }}</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    @endforeach

                    <hr class="custom-hr" style="border: none;
                    border-top: 1px dashed #000;
                    height: 0;
                    margin: 10px 0;">
                @endforeach
              
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-header bg-primary p-1"></div>
            <div class="card-body p-2" style="font-size: 12px;">
                <button class="btn btn-primary btn-sm w-100" onclick="printDiv('invoice')">Imprimer</button>
            </div>
          </div>
        </div>
    </div>

@endsection


<script>
  function printDiv(divId) {
    var divToPrint = document.getElementById(divId);
    var newWin = window.open('', 'Print-Window');
    newWin.document.open();
    newWin.document.write('<html><head><style>@page { size: A5; margin: 0; }</style></head><body>');
    newWin.document.write('<div style="padding: 0.8cm;">'); // Ajout du padding ici
    newWin.document.write(divToPrint.innerHTML);
    newWin.document.write('</body></html>');
    newWin.document.close();
    newWin.print();
    newWin.close();
}

</script>


