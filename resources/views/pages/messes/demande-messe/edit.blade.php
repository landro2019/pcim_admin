@extends('layout.master')

@section('title', 'PCIM | Messes(Modifier une demande de messe)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('messes.demandes.update', ['id' => $demande->id ]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier une demande de messe</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                          <div class="mb-2">
                            <label for="" style="font-size: 12px;">Membre</label>
                            <select name="user" class="form-select form-select-sm mt-2">
                                @if (isset($membres) && $membres !== null  && $membres !== [])
                                    <option selected hidden disabled>-- Selectionner un membre --</option>
                                    @foreach ($membres as $item)
                                        <option 
                                        value="{{ $item->id }}"
                                        @isset($demande->user)
                                          {{ $demande->user->id == $item->id ? 'selected' : '' }}
                                        @endisset
                                        >
                                          {{ $item->firstname }} {{ $item->lastname }}
                                        </option>
                                    @endforeach
                                @else
                                    <option selected hidden disabled>-- Aucun membre disponible --</option>
                                @endif
                            </select>
                          </div>
                          <div class="mb-2">
                            <div class="">
                              <label for="" style="font-size: 12px;">Catégorie</label>
                              <select name="categorie" class="form-select form-select-sm mt-2">
                                @if (isset($categories) && $categories !== null  && $categories !== [])
                                    <option selected hidden disabled>-- Selectionner une catégorie --</option>
                                    @foreach ($categories as $item)
                                        <option
                                            value="{{ $item->id }}" 
                                            @isset($demande->categorie)
                                              {{ $demande->categorie->id == $item->id ? 'selected' : '' }}
                                            @endisset
                                        >
                                          {{ $item->nom }} 
                                        </option>
                                    @endforeach
                                @else
                                    <option selected hidden disabled>-- Aucune catégorie disponible --</option>
                                @endif
                            </select>
                              
                        </div>
                          </div>
                          <div class="mb-2">
                            <label for="" style="font-size: 12px;">Message</label>
                            <textarea name="message" class="form-control form-control-sm" rows="5" placeholder="Message">{{ $demande->message }}</textarea>
                          </div>
                          <div class="mb-2">
                            <label for="" style="font-size: 12px;">Date début</label>
                            <input type="date" value="{{ Carbon\Carbon::parse($demande->dateDebut)->format('Y-m-d') }}" name="dateDebut" class="form-control form-control-sm" >
                          </div>
                          <div class="mb-2">
                            <label for="" style="font-size: 12px;">Date fin</label>
                            <input type="date" value="{{ Carbon\Carbon::parse($demande->dateFin)->format('Y-m-d') }}" name="dateFin" class="form-control form-control-sm">
                          </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('messes.demandes') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection