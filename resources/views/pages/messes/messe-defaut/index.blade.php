@extends('layout.master')

@section('title', 'PCIM | Messes par defaut')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-12">

        <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
            <div class="d-flex align-items-center">
                Voulez-vous vraiment supprimer cette mette ?
                <div class="">
                    <form id="deleteForm" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                    </form>
                </div>
                <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
            </div>
        </div>

        <script>
            function confirmDelete(itemId) {
                var route = "{{ route('messes.defauts.delete', ['id' => ':itemId']) }}";
                route = route.replace(':itemId', itemId);
                document.getElementById('deleteForm').action = route; 
                document.getElementById('confirmationAlert').style.display = 'block';
            }

            function closeAlert() {
                document.getElementById('confirmationAlert').style.display = 'none';
            }
        </script>

        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title">liste des messes par default</h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Créer une messe par défaut
                    </button>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <table class="table datatable">
              <thead>
                  <tr>
                      <th scope="col">Catégorie</th>
                      <th scope="col">Message</th>
                      <th scope="col">Statut</th>
                      <th scope="col">Action</th>
                  </tr>
              </thead>
              <tbody>
                @if (isset($defauts) && $defauts !== null && $defauts !== [])
                  @foreach ($defauts as $item)
                    <tr>
                        <td>{{ $item->categorie->nom }}</td>
                        <td>{{ $item->message }}</td>
                        <td>
                          @if ($item->etat == 'INACTIVE')
                            <span class="rounded-pill fw-semibold px-3 py-1 bg-danger-light text-danger" style="font-size: 13px;">INACTIVE</span>
                          @endif
                          @if ($item->etat == 'ACTIVE')
                            <span class="rounded-pill fw-semibold px-3 py-1 bg-success-light text-success" style="font-size: 13px;">ACTIVE</span>
                          @endif
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-2-fill"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item text-info fw-bold" href="{{ route('messes.defauts.get_byId', ['id' => $item->id ]) }}">Modifier</a></li>
                                    <li>
                                        <a class="dropdown-item text-danger fw-bold" href="#" onclick="confirmDelete({{ $item->id }})">Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                  @endforeach 
                @else
                  <tr>
                    <td colspan="7">
                      <div class="d-flex justify-content-center">
                        <div class="spinner-border me-3" role="status"></div> Aucun groupe disponible
                      </div>
                    </td>
                  </tr>
                @endif
              </tbody>
            </table>
            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>
















<!-- Basic Modal -->
  <div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('messes.defauts.create') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Créer une messe par defaut</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="mb-2">
                      <select name="categorieId" class="form-select form-select-sm mt-2">
                          @if (isset($categories) && $categories !== null  && $categories !== [])
                              <option selected hidden disabled>-- Selectionner une catégorie --</option>
                              @foreach ($categories as $item)
                                  <option value="{{ $item->id }}">{{ $item->nom }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucune catégorie disponible --</option>
                          @endif
                      </select>
                    </div> 
                    <div class="mb-2">
                      <textarea name="message" class="form-control form-control-sm" rows="5" placeholder="Message"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->

@endsection
