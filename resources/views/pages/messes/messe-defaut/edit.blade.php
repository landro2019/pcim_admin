@extends('layout.master')

@section('title', 'PCIM | Messes par défaut(Modification)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('messes.defauts.update', ['id' => $defaut->id ]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier une messe par defaut</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <label for="" style="font-size: 12px;">Catégorie</label>
                            <select name="categorieId" class="form-select form-select-sm mt-2">
                              @if (isset($categories) && $categories !== null  && $categories !== [])
                                  <option selected hidden disabled>-- Selectionner une catégorie --</option>
                                  @foreach ($categories as $item)
                                      <option
                                          value="{{ $item->id }}" 
                                          @isset($defaut->categorie)
                                            {{ $defaut->categorie->id == $item->id ? 'selected' : '' }}
                                          @endisset
                                      >
                                        {{ $item->nom }} 
                                      </option>
                                  @endforeach
                              @else
                                  <option selected hidden disabled>-- Aucune catégorie disponible --</option>
                              @endif
                          </select>
                            
                      </div>
                        <div class="">
                            <label for="" style="font-size: 12px;">Message</label>
                            <textarea name="message" class="form-control form-control-sm" rows="5" placeholder="Message">{{ $defaut->message }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('messes.defauts') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection