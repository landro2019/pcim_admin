@extends('layout.master')

@section('title', 'PCIM | Catégories de messe')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-12">

        <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
            <div class="d-flex align-items-center">
                Voulez-vous vraiment supprimer cette catégorie ?
                <div class="">
                    <form id="deleteForm" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                    </form>
                </div>
                <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
            </div>
        </div>

        <script>
            function confirmDelete(itemId) {
                var route = "{{ route('messes.categories.delete', ['id' => ':itemId']) }}";
                route = route.replace(':itemId', itemId);
                document.getElementById('deleteForm').action = route; 
                document.getElementById('confirmationAlert').style.display = 'block';
            }

            function closeAlert() {
                document.getElementById('confirmationAlert').style.display = 'none';
            }
        </script>

        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title">liste des catégories de messe</h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Créer une catégorie de messe
                    </button>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <table class="table datatable">
              <thead>
                  <tr>
                      <th scope="col">Libelle</th>
                      <th scope="col">Montant</th>
                      <th scope="col">Nombre de jour</th>
                      <th scope="col">Statut</th>
                      <th scope="col">Action</th>
                  </tr>
              </thead>
              <tbody>
                @if (isset($categories) && $categories !== null && $categories !== [])
                  @foreach ($categories as $item)
                    <tr>
                        <td>{{ $item->nom }}</td>
                        <td><nobr>{{ number_format($item->montant, 0, ',', ' ') }} F CFA</nobr></td> 
                        <td>{{ $item->nombreJour }}</td>
                        <td>
                          @if ($item->etat == 'INACTIVE')
                            <span class="rounded-pill fw-semibold px-3 py-1 bg-danger-light text-danger" style="font-size: 13px;">INACTIVE</span>
                          @endif
                          @if ($item->etat == 'ACTIVE')
                            <span class="rounded-pill fw-semibold px-3 py-1 bg-success-light text-success" style="font-size: 13px;">ACTIVE</span>
                          @endif
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-2-fill"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item text-info fw-bold" href="{{ route('messes.categories.get_byId', ['id' => $item->id ]) }}">Modifier</a></li>
                                    <li>
                                        <a class="dropdown-item text-danger fw-bold" href="#" onclick="confirmDelete({{ $item->id }})">Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                  @endforeach 
                @else
                  <tr>
                    <td colspan="5">
                      <div class="d-flex justify-content-center">
                        <div class="spinner-border me-3" role="status"></div> Aucun groupe disponible
                      </div>
                    </td>
                  </tr>
                @endif
              </tbody>
            </table>
            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>
















<!-- Basic Modal -->
  <div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('messes.categories.create') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Créer un nouveau groupe</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="mb-2">
                      <input name="nom" type="text" class="form-control form-control-sm" placeholder="Libelle">
                    </div>
                    <div class="mb-2">
                      <input name="montant" type="number" class="form-control form-control-sm" placeholder="Montant">
                    </div>
                    <div class="mb-2">
                      <input name="nombreJour" type="number" class="form-control form-control-sm" placeholder="Nombre de jour">
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->

@endsection