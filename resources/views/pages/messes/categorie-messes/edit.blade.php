@extends('layout.master')

@section('title', 'PCIM | Catégorie de messe(Modifier une catégorie)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('messes.categories.update', ['id' => $categorie->id ]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier une catégorie de messe</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <label for="" style="font-size: 12px;">Libelle</label>
                            <input name="nom" value="{{ $categorie->nom }}" type="text" class="form-control form-control-sm" placeholder="Libelle">
                        </div>
                        <div class="">
                            <label for="" style="font-size: 12px;">Montant</label>
                            <input name="montant" value="{{ $categorie->montant }}" type="number" class="form-control form-control-sm" placeholder="Montant">
                        </div>
                        <div class="">
                            <label for="" style="font-size: 12px;">Nombre de jour</label>
                            <input name="nombreJour" value="{{ $categorie->nombreJour }}" type="number" class="form-control form-control-sm" placeholder="Nombre de jour">
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('messes.categories') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection