@extends('layout.master')

@section('title', 'PCIM | Paramètres')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-6">
{{-- 
        <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
            <div class="d-flex align-items-center">
                Voulez-vous vraiment supprimer cet groupe ?
                <div class="">
                    <form id="deleteForm" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                    </form>
                </div>
                <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
            </div>
        </div>

        <script>
            function confirmDelete(itemId) {
                document.getElementById('deleteForm').action = "groupes/supprimer-un-groupe/" + itemId; // Remplacez "URL_DU_FORMULAIRE" par l'URL réelle du formulaire en ajoutant l'ID à la fin
                document.getElementById('confirmationAlert').style.display = 'block';
            }

            function closeAlert() {
                document.getElementById('confirmationAlert').style.display = 'none';
            }
        </script>
 --}}
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title me-1"><a href="{{ route('parametres.create', ['key' => 'groupe']) }}" class="btn btn-sm text-light btn-primary w-100">Générer les côtisations</a> </h5>
                </div>
                <div class="">
                  <h5 class="card-title ms-1"><button type="button" class="btn btn-sm text-light btn-success w-100" data-bs-toggle="modal" data-bs-target="#basicMod">Générer une côtisation pour un membre</button> </h5>
                </div>
                {{-- <div class=""> --}}
                    {{-- <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Créer un groupe
                    </button> --}}
                {{-- </div> --}}
            </div>
            <!-- Table with stripped rows -->
            <div class="">
              <div class="d-flex">
                
              </div>
            </div>
            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>



















  <!-- Basic Modal -->
  <div class="modal fade" id="basicMod" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('parametres.create', ['key' => 'membre']) }}" method="get">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Générer une cotisation pour un membre</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="mb-2">
                      <select name="membre" class="form-select form-select-sm mt-2">
                        @if (isset($membres) && $membres !== null  && $membres !== [])
                            <option selected hidden disabled>-- Selectionner un membre --</option>
                            @foreach ($membres as $item)
                                <option value="{{ $item->id }}">{{ $item->firstname }} {{ $item->lastname }}</option>
                            @endforeach
                        @else
                            <option selected hidden disabled>-- Aucun membre disponible --</option>
                        @endif
                    </select>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label style="font-size: 12px;">Date début</label>
                        <input type="date" name="startDate" class="form-control form-control-sm">
                      </div>
                      <div class="col-md-6">
                        <label style="font-size: 12px;">Date fin</label>
                        <input type="date" name="endDate" class="form-control form-control-sm">
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->

@endsection