@extends('layout.master')

@section('title', 'PCIM | Groupes(Modifier une cotisation)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('cotisations.update', ['id' => $cotisation->id ]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier une cotisation</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <div class="mb-2">
                                <label for="" style="font-size: 12px;">Groupe</label>
                                <select name="groupe" class="form-select form-select-sm mt-2">
                                  @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                                      <option selected hidden disabled>-- Selectionner un groupe --</option>
                                      @foreach ($groupes as $item)
                                          <option
                                              value="{{ $item->id }}" 
                                              @isset($cotisation->group)
                                                {{ $cotisation->group->id == $item->id ? 'selected' : '' }}
                                              @endisset
                                          >
                                            {{ $item->label }}
                                          </option>
                                      @endforeach
                                  @else
                                      <option selected hidden disabled>-- Aucun groupe disponible --</option>
                                  @endif
                              </select>
                                
                          </div>
                            <div class="mb-2">
                                <select name="type" class="form-select form-select-sm">
                                    <option selected disabled hidden>Selectionner un type</option>
                                    <option value="MENSUELLE" {{ $cotisation->type === 'MENSUELLE' ? 'selected' : '' }}>MENSUELLE</option>
                                    <option value="EXCEPTIONNELLE" {{ $cotisation->type === 'EXCEPTIONNELLE' ? 'selected' : '' }}>EXCEPTIONNELLE</option>
                                </select>
                            </div>
                            <div class="mb-2">
                                <input type="text" name="montant" value="{{ $cotisation->montant }}" class="form-control form-control-sm" placeholder="Montant de la cotisation">
                            </div>
                            <div class="mb-2">
                                <input type="date" name="date_start" value="{{ $cotisation->date_start }}" class="form-control form-control-sm" placeholder="Date début de cotisation">
                            </div>
                            <div class="mb-2">
                                <textarea name="description" class="form-control form-control-sm" placeholder="Description">{{ $cotisation->description }}</textarea>
                            </div>
                            <div class="mb-2">
                            <label for="" style="font-size: 12px;">Statut</label>
                            <div class="d-flex">
                                <div class="me-3">
                                    <input type="radio" name="etat" value="INVALIDE" {{ $cotisation->etat == 'INVALIDE' ? 'checked' : '' }}>
                                    <label for="">INVALIDE</label>
                                </div>
                                <div class="">
                                    <input type="radio" name="etat" value="VALIDE" {{ $cotisation->etat == 'VALIDE' ? 'checked' : '' }}>
                                    <label for="">VALIDE</label>
                                </div>
                            </div>
                            </div>
                          </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('cotisations') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection