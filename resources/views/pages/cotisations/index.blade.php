@extends('layout.master')

@section('title', 'PCIM | Cotisations')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
        <div class="d-flex align-items-center">
            Voulez-vous vraiment supprimer cette cotisation ?
            <div class="">
                <form id="deleteForm" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                </form>
            </div>
            <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
        </div>
    </div>

    <script>
        function confirmDelete(itemId) {
            document.getElementById('deleteForm').action = "cotisations/supprimer-une-cotisation/" + itemId; // Remplacez "URL_DU_FORMULAIRE" par l'URL réelle du formulaire en ajoutant l'ID à la fin
            document.getElementById('confirmationAlert').style.display = 'block';
        }

        function closeAlert() {
            document.getElementById('confirmationAlert').style.display = 'none';
        }
    </script>

        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title">liste des cotisations</h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Créer un cotisation
                    </button>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <div class="table-responsive">
              <table class="table datatable">
                <thead>
                    <tr>
                        <th scope="col">Groupe</th>
                        <th scope="col">Type</th>
                        <th scope="col">Description</th>
                        <th scope="col">Montant</th>
                        <th scope="col">Date début</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                  @if (isset($cotisations) && $cotisations !== null && $cotisations !== [])
                    @foreach ($cotisations as $item)
                      <tr>
                          <td>
                            @if ($item->group != null)
                              {{ $item->group->label }}
                            @endif
                          </td>
                          <td>{{ $item->type }}</td>
                          <td>{{ $item->description }}</td>
                          <td><nobr>{{ number_format($item->montant, 0, ',', ' ') }} F CFA</nobr></td> 
                          <td><nobr>{{ $item->date_start }}</nobr></td> 
                          <td>
                            @if ($item->etat == 'INVALIDE')
                            <span class="rounded-pill  fw-semibold px-3 py-1 bg-danger-light text-danger" style="font-size: 13px;">INVALIDE</span>
                            @endif
                            @if ($item->etat == 'VALIDE')
                              <span class="rounded-pill  fw-semibold px-3 py-1 bg-success-light text-success" style="font-size: 13px;">VALIDE</span>
                            @endif
                          </td>
                          <td>
                              <div class="dropdown">
                                  <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                      <i class="ri-more-2-fill"></i>
                                  </button>
                                  <ul class="dropdown-menu">
                                      <li><a class="dropdown-item text-info fw-bold" href="{{ route('cotisations.get_byId', ['id' => $item->id ]) }}">Modifier</a></li>
                                      <li>
                                        <a class="dropdown-item text-danger fw-bold" href="#" onclick="confirmDelete({{ $item->id }})">Supprimer</a>
                                    </li>
                                  </ul>
                              </div>
                          </td>
                      </tr>
                    @endforeach 
                  @else
                    <tr>
                      <td colspan="7">
                        <div class="d-flex justify-content-center">
                          <div class="spinner-border me-3" role="status"></div> Aucune cotisation disponible
                        </div>
                      </td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>

            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>
















<!-- Basic Modal -->
<div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('cotisations.create') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Créer une nouvelle cotisation</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                  <div class="mb-2">
                    <div class="mb-2">
                      <select name="groupe" class="form-select form-select-sm mt-2">
                          @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                              <option selected hidden disabled>-- Selectionner un groupe --</option>
                              @foreach ($groupes as $item)
                                  <option value="{{ $item->id }}">{{ $item->label }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucun groupe disponible --</option>
                          @endif
                      </select>
                  </div> 
                    <div class="mb-2">
                        <select name="type" class="form-select form-select-sm">
                          <option selected disabled hidden>Selectionner un type</option>
                          <option value="MENSUELLE">MENSUELLE</option>
                          <option value="	EXCEPTIONNELLE">EXCEPTIONNELLE</option>
                        </select>
                    </div>
                    <div class="mb-2">
                      <input type="text" name="montant" class="form-control form-control-sm" placeholder="Montant de la cotisation">
                    </div>
                    <div class="mb-2">
                      <input type="date" name="date_start" class="form-control form-control-sm" placeholder="Date début de cotisation">
                    </div>
                    <div class="mb-2">
                      <textarea name="description" class="form-control form-control-sm" placeholder="Description"></textarea>
                    </div>
                    <div class="mb-2">
                      <label for="" style="font-size: 12px;">Statut</label>
                      <div class="d-flex">
                        <div class="me-3">
                          <input type="radio" name="etat" value="INVALIDE" checked>
                          <label for="">INVALIDE</label>
                        </div>
                        <div class="">
                          <input type="radio" name="etat" value="VALIDE">
                          <label for="">VALIDE</label>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->

@endsection