                <!-- Reports -->
                <div class="col-12">
                    <div class="card">
        
                        <div class="filter">
                            <div class="me-3">
                                <select name="categoryId" id="selectCategory" class="form-select form-select-sm">
                                    @if (isset($categories) && $categories !== null  && $categories !== [])
                                        @foreach ($categories as $item)
                                            <option value="{{ $item->id }}">{{ $item->nom }}</option>
                                        @endforeach
                                    @else
                                        <option selected hidden disabled>-- Aucune categorie disponible --</option>
                                    @endif
                                </select>
                            </div>
                        </div>
        
                        <div class="card-body">
                        <h5 class="card-title">Evolution des demandes de messe par mois et par catégorie ( Année: {{ session('year') }} )</h5>
        
                        <!-- Line Chart -->
                        <div id="reportsChartMes"></div>
        
                        <script>
                            showGraphMes();





                            const moisDeLAnneeMes = [
                                '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'
                            ];

                            // Fonction pour compléter les montants manquants avec 0
                            function completerMontantsMes(data) {
                                const montantsCompletsMes = [];
                                const etatsPossiblesMes = ['PAYE', 'IMPAYE'];

                                for (const mois of moisDeLAnneeMes) {
                                    for (const etat of etatsPossiblesMes) {
                                        const existingEntry = data.find(entry => entry.mois === mois && entry.etat === etat);
                                        if (existingEntry) {
                                            montantsCompletsMes.push(existingEntry);
                                        } else {
                                            montantsCompletsMes.push({ mois, etat, montant: 0 });
                                        }
                                    }
                                }

                                return montantsCompletsMes;
                            }

                            

                            var graphDataMes = [];
                            var moisImpayes = [0,0,0,0,0,0,0,0,0,0,0,0];
                            var moisPayes = [0,0,0,0,0,0,0,0,0,0,0,0];
                            var donneMoisImpayesMes = [];
                            var donneMoisPayesMes = [];
                            var chartMes ;
                            var firstCategoryId = $('#selectCategory').val();

                            getDataApiMes(firstCategoryId);
                            $('#selectCategory').on('change',function(){
                                getDataApiMes(this.value,);
                            })

                            function getDataApiMes(categoryId) {

                                $.ajax({
                                    url: 'graphMesse/'+categoryId,
                                    type: 'GET',
                                    dataType: 'json',
                                    success: function(data) {
                                        
                                        donneMoisImpayesMes =[];
                                        donneMoisPayesMes =[];
                                        graphDataMes = data.statistique.graphData
                                        
                                        const dataCompletesMes = completerMontantsMes(graphDataMes);

                                        dataCompletesMes.forEach(element => {
                                            if (element.etat == 'IMPAYE') {
                                                donneMoisImpayesMes.push(parseInt(element.montant))
                                            } else {
                                                donneMoisPayesMes.push(parseInt(element.montant))
                                            }

                                        });

                                        chartMes.updateSeries([{                
                                            name: 'Payés',
                                            data: donneMoisPayesMes
                                        },
                                        {                
                                            name: 'Impayés',
                                            data: donneMoisImpayesMes
                                        }]); 


                                    },
                                    error:function(error){
                                        console.log(error);
                                    }
                                }); 
                            }





                            function showGraphMes() {
                                var settings = {
                                    series: [{
                                        name: 'Payés',
                                        data: [0,0,0,0,0,0,0,0,0,0,0,0]
                                    }, {
                                        name: 'Impayés',
                                        data: [0,0,0,0,0,0,0,0,0,0,0,0]
                                    }],
                                    chart: {
                                        height: 350,
                                        type: 'area',
                                        toolbar: {
                                        show: false
                                        },
                                    },
                                    markers: {
                                        size: 4
                                    },
                                    colors: ['#2eca6a', '#ff771d'],
                                    fill: {
                                        type: "gradient",
                                        gradient: {
                                        shadeIntensity: 1,
                                        opacityFrom: 0.3,
                                        opacityTo: 0.4,
                                        stops: [0, 90, 100]
                                        }
                                    },
                                    dataLabels: {
                                        enabled: false
                                    },
                                    stroke: {
                                        curve: 'smooth',
                                        width: 2
                                    },
                                    xaxis: {
                                        type: 'month',
                                        categories: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin",  "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" ]
                                    },
                                    tooltip: {
                                        x: {
                                        format: 'mm'
                                        },
                                    }
                                };

                                console.log(settings);
                
                                document.addEventListener("DOMContentLoaded", () => {
                                    chartMes = new ApexCharts(document.querySelector("#reportsChartMes"), settings);
                                    try {
                                        chartMes.destroy();
                                    } catch (error) {
                                        
                                    }
                                    chartMes = new ApexCharts(document.querySelector("#reportsChartMes"), settings);
                                    chartMes.render();
                                
                                });
                            }
                        </script>
                        <!-- End Line Chart -->
        
                        </div>
        
                    </div>
                </div><!-- End Reports -->