                <!-- Reports -->
                <div class="col-12">
                    <div class="card">

                    <div class="filter">
                        <div class="me-3">
                            <select name="groupId" id="selectGroup" class="form-select form-select-sm">
                                @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                                    @foreach ($groupes as $item)
                                        <option value="{{ $item->id }}">{{ $item->label }}</option>
                                    @endforeach
                                @else
                                    <option selected hidden disabled>-- Aucun groupe disponible --</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="card-body">
                        <h5 class="card-title">Evolution des cotisations par mois et par groupe ( Année: {{ session('year') }} )</h5>

                        <!-- Line Chart -->
                        <div id="reportsChart"></div>

                        <script>

                            showGraph();

                            const moisDeLAnnee = [
                                '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'
                            ];

                            // Fonction pour compléter les montants manquants avec 0
                            function completerMontants(data) {
                                const montantsComplets = [];
                                const etatsPossibles = ['PAYE', 'IMPAYE'];

                                for (const mois of moisDeLAnnee) {
                                    for (const etat of etatsPossibles) {
                                        const existingEntry = data.find(entry => entry.mois === mois && entry.etat === etat);
                                        if (existingEntry) {
                                            montantsComplets.push(existingEntry);
                                        } else {
                                            montantsComplets.push({ mois, etat, montant: 0 });
                                        }
                                    }
                                }

                                return montantsComplets;
                            }

                            

                            var graphData = [];
                            var moisImpayes = [0,0,0,0,0,0,0,0,0,0,0,0];
                            var moisPayes = [0,0,0,0,0,0,0,0,0,0,0,0];
                            var donneMoisImpayes = [];
                            var donneMoisPayes = [];
                            var chart ;
                            var firstGroupId = $('#selectGroup').val();
                            getDataApi(firstGroupId);
                            $('#selectGroup').on('change',function(){
                    
                                getDataApi(this.value,);

                            })

                            function getDataApi(groupId) {
                                $.ajax({
                                url: 'graph/'+groupId,
                                type: 'GET',
                                dataType: 'json',
                                success: function(data) {
                                    
                                donneMoisImpayes =[];
                                donneMoisPayes =[];
                                graphData = data.statistique.graphData
                                
                                const dataCompletes = completerMontants(graphData);
                                dataCompletes.forEach(element => {
                                    if (element.etat == 'IMPAYE') {
                                        donneMoisImpayes.push(parseInt(element.montant))
                                    } else {
                                        donneMoisPayes.push(parseInt(element.montant))
                                    }

                                });

                                chart.updateSeries([{                
                                        name: 'Payés',
                                        data: donneMoisPayes
                                    },
                                    {                
                                        name: 'Impayés',
                                        data: donneMoisImpayes
                                    }]);

                                },
                                error:function(error){
                                    console.log(error);
                                }
                            }); 
                            }



                        function showGraph() {

                            var options ={
                                series: [ {
                                    name: 'Payés',
                                    data: [0,0,0,0,0,0,0,0,0,0,0,0]
                                }, {
                                    name: 'Impayés',
                                    data: [0,0,0,0,0,0,0,0,0,0,0,0]
                                }],
                                chart: {
                                    height: 350,
                                    type: 'area',
                                    toolbar: {
                                    show: false
                                    },
                                },
                                markers: {
                                    size: 4
                                },
                                colors: [ '#2eca6a', '#ff771d'],
                                fill: {
                                    type: "gradient",
                                    gradient: {
                                    shadeIntensity: 1,
                                    opacityFrom: 0.3,
                                    opacityTo: 0.4,
                                    stops: [0, 90, 100]
                                    }
                                },
                                dataLabels: {
                                    enabled: false
                                },
                                stroke: {
                                    curve: 'smooth',
                                    width: 2
                                },
                                xaxis: {
                                    type: 'month',
                                    categories: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin",  "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" ]
                                },
                                tooltip: {
                                    x: {
                                    format: 'mm'
                                    },
                                }
                            };

                            console.log(options);
                            

                            document.addEventListener("DOMContentLoaded", () => {
                                chart = new ApexCharts(document.querySelector("#reportsChart"), options);
                                try {
                                    chart.destroy();
                                } catch (error) {
                                    
                                }
                                chart = new ApexCharts(document.querySelector("#reportsChart"), options);
                                chart.render();
                            
                            });
                        }
                        </script>
                        <!-- End Line Chart -->

                    </div>

                    </div>
                </div><!-- End Reports -->