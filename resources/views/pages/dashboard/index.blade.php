@extends('layout.master')

@section('title', 'PCIM | Tableau de bord')

@section('content')

<section class="section dashboard">
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-12">
            <div class="row">

                <!-- Sales Card -->
                <div class="col-xxl-3 col-lg-3">
                    <div class="card info-card sales-card">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Membres <span></span></h5>
                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-people"></i>
                                </div>
                                <div class="ps-3">
                                    <h6><span style="font-size:25px;">{{ number_format($statistiques['users'], 0, ',', ' ') }}</span></h6>
                                    {{-- <span class="text-danger small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">decrease</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Sales Card -->

                <div class="col-xxl-9 col-lg-9">
                    <div class="row">
                        <!-- Revenue Card -->
                        <div class="col-xxl-6 col-lg-6">
                            <div class="card info-card revenue-card">

                                <div class="filter">
                                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                        <li class="dropdown-header text-start"><h6>Filter</h6></li>
                                        <li><a class="dropdown-item" href="#" onclick="statisticMesse('mois')">Mois en cours</a></li>
                                        <li><a class="dropdown-item" href="#" onclick="statisticMesse('annees')">Année en cours</a></li>
                                    </ul>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title" id="card-title-Messe">
                                        <div class="d-flex align-items-center">
                                            Messes
                                            <span id="titleAnneeMesse" class="ms-2">| Année en cours</span>
                                            <span id="titleMoisMesse" class="ms-2" style="display: none;">| Mois en cours</span>
                                        </div>
                                    </h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-currency-dollar"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>
                                                <span id="moisMesse" style="display: none; font-size:25px;">{{ number_format($statistiquesMesse['mois'], 0, ',', ' ') }} F CFA</span>
                                                <span id="anneesMesse" style="font-size:25px;">{{ number_format($statistiquesMesse['annees'], 0, ',', ' ') }} F CFA</span>
                                            </h6>
                                            {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span class="text-muted small pt-2 ps-1">increase</span> --}}
                                            <script>
                                                function statisticMesse(item) {
                                                    let ms = document.querySelector('#moisMesse');
                                                    let ans = document.querySelector('#anneesMesse');
                                                    let tms = document.querySelector('#titleMoisMesse');
                                                    let tans = document.querySelector('#titleAnneeMesse');
                                                    var cardTitleMesse = document.querySelector('#card-title-Messe'); // Correction ici
                                                    
                                                    if (item == 'mois') {
                                                        ms.style.display='block';
                                                        ans.style.display='none';
                                                        tms.style.display='block';
                                                        tans.style.display='none';
                                                        cardTitleMesse.style.whiteSpace = 'nowrap';
                                                    }
                                                    if (item == 'annees') {
                                                        ms.style.display='none';
                                                        ans.style.display='block';
                                                        tms.style.display='none';
                                                        tans.style.display='block';
                                                        cardTitleMesse.style.whiteSpace = 'nowrap';
                                                    }
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Revenue Card -->

                        <!-- Revenue Card -->
                        <div class="col-xxl-6 col-lg-6">
                            <div class="card info-card revenue-card">

                                <div class="filter">
                                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                        <li class="dropdown-header text-start"><h6>Filter</h6></li>
                                        <li><a class="dropdown-item" href="#" onclick="statistic('mois')">Mois en cours</a></li>
                                        <li><a class="dropdown-item" href="#" onclick="statistic('annees')">Année en cours</a></li>
                                    </ul>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title" id="card-title">
                                        <div class="d-flex align-items-center">
                                            Coltisations
                                            <span id="titleAnnee" class="ms-2">| Année en cours</span>
                                            <span id="titleMois" class="ms-2" style="display: none;">| Mois en cours</span>
                                        </div>
                                    </h5>

                                    <div class="d-flex align-items-center">
                                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-currency-dollar"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>
                                                <span id="mois" style="display: none; font-size:25px;">{{ number_format($statistiques['mois'], 0, ',', ' ') }} F CFA</span>
                                                <span id="annees" style="font-size:25px;">{{ number_format($statistiques['annees'], 0, ',', ' ') }} F CFA</span>
                                            </h6>
                                            {{-- <span class="text-success small pt-1 fw-bold">8%</span> <span class="text-muted small pt-2 ps-1">increase</span> --}}
                                            <script>
                                                function statistic(item) {
                                                    let ms = document.querySelector('#mois');
                                                    let ans = document.querySelector('#annees');
                                                    let tms = document.querySelector('#titleMois');
                                                    let tans = document.querySelector('#titleAnnee');
                                                    var cardTitle = document.querySelector('#card-title'); // Correction ici
                                                    
                                                    if (item == 'mois') {
                                                        ms.style.display='block';
                                                        ans.style.display='none';
                                                        tms.style.display='block';
                                                        tans.style.display='none';
                                                        cardTitle.style.whiteSpace = 'nowrap';
                                                    }
                                                    if (item == 'annees') {
                                                        ms.style.display='none';
                                                        ans.style.display='block';
                                                        tms.style.display='none';
                                                        tans.style.display='block';
                                                        cardTitle.style.whiteSpace = 'nowrap';
                                                    }
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Revenue Card -->
                    </div>
                </div>



                @include('pages.dashboard.graphs.graphCotisation')

                @include('pages.dashboard.graphs.graphMesse')

            </div>
        </div><!-- End Left side columns -->

    </div>
</section>

@endsection