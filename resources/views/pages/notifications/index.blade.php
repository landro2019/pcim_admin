@extends('layout.master')

@section('title', 'PCIM | Notifications')

@section('content')

<section class="section">
  <div class="row">
    <div class="col-lg-12">

        <div class="p-3 alert alert-warning" id="confirmationAlert" style="display: none;">
            <div class="d-flex align-items-center">
                Voulez-vous vraiment supprimer cette notification ?
                <div class="">
                    <form id="deleteForm" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger ms-3">OUI</button>
                    </form>
                </div>
                <button class="btn btn-sm btn-primary ms-2" onclick="closeAlert()">NON</button>
            </div>
        </div>

        <script>
            function confirmDelete(itemId) {
                document.getElementById('deleteForm').action = "notifications/supprimer-une-notification/" + itemId; // Remplacez "URL_DU_FORMULAIRE" par l'URL réelle du formulaire en ajoutant l'ID à la fin
                document.getElementById('confirmationAlert').style.display = 'block';
            }

            function closeAlert() {
                document.getElementById('confirmationAlert').style.display = 'none';
            }
        </script>

        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h5 class="card-title">liste des notifications</h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-sm text-light" style="background-color: #4b5cf2f1;" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Créer une notification
                    </button>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <table class="table datatable">
              <thead>
                  <tr>
                      <th scope="col">Libelle</th>
                      <th scope="col">Lancement</th>
                      <th scope="col">Description</th>
                      <th scope="col">Actions</th>
                  </tr>
              </thead>
              <tbody>
                @if (isset($notifications) && $notifications !== null && $notifications !== [])
                  @foreach ($notifications as $item)
                    <tr>
                        <td>{{ $item->libelle }}</td>
                        <td>{{ Carbon\Carbon::parse($item->date_lancement)->format('d-m-Y') }}</td> 
                        <td>{{ $item->description }}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-2-fill"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="dropdown-item text-danger fw-bold" href="#" onclick="confirmDelete({{ $item->id }})">Supprimer</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                  @endforeach 
                @else
                  <tr>
                    <td colspan="4">
                      <div class="d-flex justify-content-center">
                        <div class="spinner-border me-3" role="status"></div> Aucune notification disponible
                      </div>
                    </td>
                  </tr>
                @endif
              </tbody>
            </table>
            <!-- End Table with stripped rows -->
          </div>
        </div>

    </div>
  </div>
</section>















<!-- Basic Modal -->
  <div class="modal fade" id="basicModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="basicModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('notifications.create') }}" method="post">
          @csrf
            <div class="modal-header py-2">
                <h5 class="modal-title">Créer une nouvelle notification</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="mb-2">
                      <select name="groupe" class="form-select form-select-sm mt-2">
                          @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                              <option selected hidden disabled>-- Selectionner un groupe --</option>
                              @foreach ($groupes as $item)
                                  <option value="{{ $item->id }}">{{ $item->label }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucun groupe disponible --</option>
                          @endif
                      </select>
                    </div>
                    <div class="mb-2">
                      <select name="membre" class="form-select form-select-sm mt-2">
                          @if (isset($membres) && $membres !== null  && $membres !== [])
                              <option selected hidden disabled>-- Selectionner un membre --</option>
                              @foreach ($membres as $item)
                                  <option value="{{ $item->id }}">{{ $item->firstname }} {{ $item->lastname }}</option>
                              @endforeach
                          @else
                              <option selected hidden disabled>-- Aucun membre disponible --</option>
                          @endif
                      </select>
                    </div> 
                    <div class="mb-2">
                        <input name="libelle" type="text" class="form-control form-control-sm" placeholder="Libelle">
                    </div>
                    <div class="mb-2">
                      <input name="date_lancement" type="date" class="form-control form-control-sm" placeholder="Date de lancement">
                    </div>
                    <div class="">
                      <textarea name="description" class="form-control form-control-sm" placeholder="Description"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Fermer</button>
                <button type="reset" class="btn btn-sm btn-danger">Annuler</button>
                <button type="submit" class="btn btn-sm btn-primary">Créer</button>
            </div>
        </form>
      </div>
    </div>x
  </div><!-- End Basic Modal-->

@endsection