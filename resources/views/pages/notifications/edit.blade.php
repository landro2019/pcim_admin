@extends('layout.master')

@section('title', 'PCIM | Notifications(Modifier une notification)')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('notifications.update', ['id' => $notification->id ]) }}" method="post">
                @method('PUT')
                @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="title-card">Modifier une notification</div>
                    </div>
                    <div class="card-body pb-1">
                        <div class="">
                            <div class="mb-2">
                              <label for="" style="font-size: 12px;">Groupe</label>
                              <select name="groupe" class="form-select form-select-sm mt-2">
                                  @if (isset($groupes) && $groupes !== null  && $groupes !== [])
                                      <option selected hidden disabled>-- Selectionner un groupe --</option>
                                      @foreach ($groupes as $item)
                                          <option value="{{ $item->id }}" {{  $item->id == $notification->group_id ? 'selected' : ''}}>{{ $item->label }}</option>
                                      @endforeach
                                  @else
                                      <option selected hidden disabled>-- Aucun groupe disponible --</option>
                                  @endif
                              </select>
                            </div>
                            <div class="mb-2">
                              <label for="" style="font-size: 12px;">Membre</label>
                              <select name="membre" class="form-select form-select-sm mt-2">
                                  @if (isset($membres) && $membres !== null  && $membres !== [])
                                      <option selected hidden disabled>-- Selectionner un membre --</option>
                                      @foreach ($membres as $item)
                                          <option value="{{ $item->id }}" {{  $item->id == $notification->user_id ? 'selected' : ''}}>{{ $item->firstname }} {{ $item->lastname }}</option>
                                      @endforeach
                                  @else
                                      <option selected hidden disabled>-- Aucun membre disponible --</option>
                                  @endif
                              </select>
                            </div>
                            <div class="mb-2">
                                <label for="" style="font-size: 12px;">Libelle</label>
                                <input name="libelle" value="{{ $notification->libelle }}" type="text" class="form-control form-control-sm" placeholder="Libelle">
                            </div>
                            <div class="mb-2">
                                <label for="" style="font-size: 12px;">Date de lancement</label>
                                <input name="date_lancement" value="{{ $notification->date_lancement }}" type="date" class="form-control form-control-sm" placeholder="Date de lancement">
                            </div>
                            <div class="">
                                <label for="" style="font-size: 12px;">Description</label>
                                <textarea name="description" class="form-control form-control-sm" placeholder="Description">{{ $notification->description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center justify-content-end">
                            <a href="{{ route('notifications') }}" class="btn btn-danger btn-sm">Annuler</a>
                            <button type="submit" class="btn btn-sm btn-primary ms-2">Modifier</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
