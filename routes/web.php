<?php

use App\Http\Controllers\AnnonceController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategorieMesseController;
use App\Http\Controllers\CotisationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DefautMesseController;
use App\Http\Controllers\DemandeMesseController;
use App\Http\Controllers\MembreController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ParametreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::view('/', 'pages.auth.login')->name('login.view');
Route::post('connexion',[AuthController::class,'login'])->name('login');
Route::get('déconnexion',[AuthController::class,'logout'])->name('logout');
Route::get('tableau-de-bord',[DashboardController::class,'index'])->name('dashboard');
Route::get('statistics',[DashboardController::class,'statistics'])->name('dashboard.statistics');
Route::get('graph/{groupId}',[DashboardController::class,'graph']);
Route::get('graphMesse/{categoryId}',[DashboardController::class,'graphMesse']);

Route::controller(GroupeController::class)->prefix('groupes')->name('groupes')->group(function () {
    Route::get('/', 'index');
    Route::post('créer-un-groupe', 'create')->name('.create');
    Route::get('modifier-un-groupe/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
    Route::patch('modifier-un-groupe/{id}', 'update')->whereNumber('id')->name('.update');
    Route::delete('supprimer-un-groupe/{id}', 'delete')->whereNumber('id')->name('.delete');
});

Route::controller(MembreController::class)->prefix('membres')->name('membres')->group(function () {
    Route::get('/', 'index');
    Route::post('créer-un-membre', 'create')->name('.create');
    Route::get('modifier-detail-un-membre/{id}/{key}', 'get_byId')->whereNumber('id')->whereAlpha('key')->name('.get_byId');
    Route::patch('modifier-un-membre/{id}', 'update')->whereNumber('id')->name('.update');
    Route::put('réinitialiser-un-membre/{id}', 'reset')->whereNumber('id')->name('.reset');
    Route::put('paye-un-membre/{id}/{key}', 'paye')->whereNumber('id')->whereAlpha('key')->name('.paye');
    Route::post('paye-un-membre', 'payeM')->name('.payeM');
    Route::get('status-paye-un-membre/{id}', 'paye_status')->whereNumber('id')->name('.paye_status');
    Route::put('role-membre/{id}', 'role')->whereNumber('id')->name('.role');
});

Route::controller(CotisationController::class)->prefix('cotisations')->name('cotisations')->group(function () {
    Route::get('/', 'index');
    Route::post('créer-une-cotisation', 'create')->name('.create');
    Route::get('modifier-une-cotisation/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
    Route::put('modifier-une-cotisation/{id}', 'update')->whereNumber('id')->name('.update');
    Route::delete('supprimer-une-cotisation/{id}', 'delete')->whereNumber('id')->name('.delete');
    Route::delete('supprimer-une-cotisation-membre/{id}', 'deleteUser')->whereNumber('id')->name('.delete.user');
});

Route::controller(AnnonceController::class)->prefix('annonces')->name('annonces')->group(function () {
    Route::get('/', 'index');
    Route::post('créer-une-annonce', 'create')->name('.create');
    Route::get('modifier-une-annonce/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
    Route::put('modifier-une-annonce/{id}', 'update')->whereNumber('id')->name('.update');
    Route::delete('supprimer-une-annonce/{id}', 'delete')->whereNumber('id')->name('.delete');
});

Route::controller(NotificationController::class)->prefix('notifications')->name('notifications')->group(function () {
    Route::get('/', 'index');
    Route::post('créer-une-notification', 'create')->name('.create');
    Route::get('modifier-une-notification/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
    Route::put('modifier-une-notification/{id}', 'update')->whereNumber('id')->name('.update');
    Route::delete('supprimer-une-notification/{id}', 'delete')->whereNumber('id')->name('.delete');
});

Route::prefix('messes')->name('messes')->group(function () {
    Route::controller(CategorieMesseController::class)->prefix('categories')->name('.categories')->group(function () {
        Route::get('/', 'index');
        Route::post('créer-une-catégorie', 'create')->name('.create');
        Route::get('modifier-une-catégorie/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
        Route::put('modifier-une-catégorie/{id}', 'update')->whereNumber('id')->name('.update');
        Route::delete('supprimer-une-catégorie/{id}', 'delete')->whereNumber('id')->name('.delete');
    });
    Route::controller(DefautMesseController::class)->prefix('defauts')->name('.defauts')->group(function () {
        Route::get('/', 'index');
        Route::post('création', 'create')->name('.create');
        Route::get('modification/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
        Route::put('modification/{id}', 'update')->whereNumber('id')->name('.update');
        Route::delete('suppression/{id}', 'delete')->whereNumber('id')->name('.delete');
    });
    Route::controller(DemandeMesseController::class)->prefix('demandes')->name('.demandes')->group(function () {
        Route::get('/', 'index');
        Route::post('création', 'create')->name('.create');
        Route::get('modification/{id}', 'get_byId')->whereNumber('id')->name('.get_byId');
        Route::get('détail/{id}', 'detail')->whereNumber('id')->name('.detail');
        Route::put('modification/{id}', 'update')->whereNumber('id')->name('.update');
        Route::put('célébration/{id}', 'celebre')->whereNumber('id')->name('.celebre');
        Route::put('paye/{id}', 'paye')->whereNumber('id')->name('.paye');
        Route::post('paiement', 'payeM')->name('.payeM');
        Route::get('status-paye-demande/{id}', 'paye_status')->whereNumber('id')->name('.paye_status');
        Route::delete('suppression/{id}', 'delete')->whereNumber('id')->name('.delete');
        Route::post('facture', 'facture')->name('.facture');
    });
});

Route::controller(ParametreController::class)->prefix('parametres')->name('parametres')->group(function () {
    Route::get('/', 'index');
    Route::get('générer-une-cotisation/{key}', 'create')->name('.create');
});


// detail de demande messes 
// btn d'omprimer a droite++++++++++
// afficher l'id dans le tableau preceder de #
// affiche category dans imprimer