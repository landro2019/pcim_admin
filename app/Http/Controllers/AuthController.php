<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'users/login',[
                'headers' => [
                    'Accept'    => 'application/json',
                    'X-api-key' =>  env('API_KEY')
                ],
                'form_params' => [
                    'phone'    => $inputs['phone'],
                    'password' => $inputs['password'],
                ]
            ]);
            $body = json_decode($response->getBody());
            if ($body->role != 'admin') {
                return redirect()->back()->with('error','Accès réfusé !'); 
            }
            /* 
            Session::put('user', $body->user);
            Session::put('token', $body->access_token);
            */
            session([
                'user' => $body->user,
                'token' => $body->access_token
            ]);
            
            return redirect()->route('dashboard');
            return to_route('dashboard');

        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            $decodedBody = json_decode($body, true); 
            if ($decodedBody['statusCode'] == 401) {
                return redirect()->back()->with('error','Erreur d\'authentification !'); 
            }
            if ($decodedBody['statusCode'] == 404) {
                return redirect()->back()->with('error','Utilisateur introuvable !'); 
            }
            $messageErreurDecode = html_entity_decode($decodedBody['message'] ?? $decodedBody['error']); 
            return redirect()->back()->with('error',$messageErreurDecode); 
        } catch (RequestException $e) {
            // Gérer les erreurs de requête (par exemple, problème de connexion au serveur)
            return redirect()->back()->with('error', 'Erreur de requête : ' . $e->getMessage());
        }
    }

    /**
     * Supprimer une session spécifique
     * session()->forget('user');
     * 
     * Supprimer toutes les sessions
     * session()->flush();
     */

    public function logout() {
        session()->flush();
        return to_route('login.view')->with('message','Déconnexion réussie');
    }
/* 
    public function logout()
    {
        if (!session('user') && !session('token')) {
            session()->flush();
            return to_route('login.view')->with('success','Déconnexion réussie');
        }
        try {
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'/logout',[
                'headers' => [
                    'Accept'        => 'application/json',
                    'X-api-key'     =>  env('API_KEY'),
                    'Authorization' => 'Bearer '.session('token')
                ]
            ]);

            $body = json_decode($response->getBody());
            session()->flush();
            return to_route('login.view')->with('success',$body->message);

        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            $decodedBody = json_decode($body, true); 
            $messageErreurDecode = html_entity_decode($decodedBody['message'] ?? $decodedBody['error']); 

            if (strpos($messageErreurDecode, 'Unauthenticated') !== false) {
                session()->flush();
                return to_route('login.view')->with('success','Déconnexion réussie');
            } else {
                return redirect()->back()->with('error', $messageErreurDecode);
            }
        }
    }
 */    
}
