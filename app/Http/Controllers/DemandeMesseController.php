<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use App\Http\Requests\DemandeMesseRequest;
use GuzzleHttp\Exception\RequestException;

class DemandeMesseController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'users/0/100',['headers' => $this->verifHeaders() ]);
            $membres = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'categorie-messes',['headers' => $this->verifHeaders()]);
            $categories = json_decode($response->getBody()); 

            $response = $client->request('GET', env('ENDPOINT').'demande-messes',['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            return view('pages.messes.demande-messe.index')->with([
                'demamdes'   => $body,
                'membres'    => $membres,
                'categories' => $categories,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function create(DemandeMesseRequest $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'demande-messes',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'user'      => ['id' => (int)($inputs['user'])] ,
                    'categorie' => ['id' => (int)($inputs['categorie'])] ,
                    'message'   => $inputs['message'],
                    'dateDebut' => $inputs['dateDebut'],
                    'dateFin'   => $inputs['dateDebut'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Demamde de messe créée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'demande-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Demamde de messe supprimée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'users/0/100',['headers' => $this->verifHeaders() ]);
            $membres = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'categorie-messes',['headers' => $this->verifHeaders()]);
            $categories = json_decode($response->getBody()); 

            $response = $client->request('GET', env('ENDPOINT').'demande-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            return view('pages.messes.demande-messe.edit')->with([
                'demande'    => $body,
                'categories' => $categories,
                'membres'    => $membres,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function detail($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'demande-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            //dd($body);

            return view('pages.messes.demande-messe.detail')->with([
                'demande' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(DemandeMesseRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'demande-messes/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'user'      => ['id' => (int)($inputs['user'])] ,
                    'categorie' => ['id' => (int)($inputs['categorie'])] ,
                    'message'   => $inputs['message'],
                    'dateDebut' => $inputs['dateDebut'],
                    'dateFin'   => $inputs['dateFin'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return to_route('messes.demandes')->with('message', 'Demande de messe mise à jour avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function paye($id)
    {
        try {
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'demande-messes/payment/espece/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
                
            if ($body->statusCode == 200) {
                return redirect()->back()->with('message', 'Paiement effectué avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de paiement, veuillez réessayer !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function payeM(Request $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'demande-messes/payment',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'id'    => $inputs['idCotisation'],
                    'phone' => $inputs['phone'], 
                ]
            ]);
            $body = json_decode($response->getBody());
            //dd($body);
            if (isset($body->data)) {
                return redirect()->back()->with('payment', $body->data);
            } else {
                return redirect()->back()->with('error', 'Echec de l\'opération !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function celebre($id)
    {
        try {
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'demande-messes/celebration/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
                
            if ($body->statusCode == 200) {
                return redirect()->back()->with('message', 'Célébration effectué avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de paiement, veuillez réessayer !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function paye_status($id) {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'demande-messes/payment/status/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            if (isset($body->data)) {
                return redirect()->back()->with('message', 'Votre opération est en cours de traitement !');
            } else {
                return redirect()->back()->with('error', 'Echec de l\'opération !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function factureGet($date="") {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'demande-messes/date/'.$date,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
//dd($body);
            $groupedData = [];
            foreach ($body as $item) {
                if ($item->etat == 'PAYE' && $item->celebre == 'NON') {

                    $username = "[".$item->user->id."]".$item->user->firstname." ".$item->user->lastname;
                    if (!isset($groupedData[$username])){
                        $groupedData[$username] = [];
                    }
                    $groupedData[$username][] = $item;
                }
            }

            return view('pages.messes.demande-messe.facture')->with([
                'groupedData' => $groupedData
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function facture(Request $request)
    {
        try {
            $inputs = $request->all();
            if (empty($inputs['date'])) {
                return redirect()->back()->with('error', 'Veuillez d\'abord selectionner une date !');
            }
            return $this->factureGet($inputs['date']);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
