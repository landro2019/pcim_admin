<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\AnnonceRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class AnnonceController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'annonces/0/100',[
                'headers' => $this->verifHeaders()
            ]);
            $body = json_decode($response->getBody());
            return view('pages.annonces.index')->with([
                'annonces' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
    

    

    public function create(AnnonceRequest $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'annonces',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'titre'   => $inputs['titre'],
                    'auteur'  => $inputs['auteur'],
                    'message' => $inputs['message'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Message créé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'annonces/'.$id,[
                'headers' => $this->verifHeaders(),
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Annonce supprimé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'annonces/'.$id,[
                'headers' => $this->verifHeaders(),
            ]);
            $body = json_decode($response->getBody());
            return view('pages.annonces.edit')->with([
                'annonce' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(AnnonceRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'annonces/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'titre'   => $inputs['titre'],
                    'auteur'  => $inputs['auteur'],
                    'message' => $inputs['message'],
                ]
            ]);
            $body = json_decode($response->getBody());

            if ($body->statusCode == 200) {
                return to_route('annonces')->with('message', 'Annonce mise à jour avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de mise à jour !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
