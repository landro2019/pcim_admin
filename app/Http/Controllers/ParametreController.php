<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class ParametreController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'users/0/100',['headers' => $this->verifHeaders()]);
            $membres = json_decode($response->getBody());

            return view('pages.parametres.index')->with([
                'membres'     => $membres,
                'groupes'     => $groupes,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function create(Request $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            if ($request->key == 'groupe') {
                $response = $client->request('GET', env('ENDPOINT').'cotisations/generate',[
                    'headers' => $this->verifHeaders(),
                ]);
            } else {
                $startDate = Carbon::parse($inputs['startDate'])->format('Y-m-d');
                $endDate   = Carbon::parse($inputs['endDate'])->format('Y-m-d');
                //dd($inputs['startDate'],$inputs['endDate'],$inputs['membre']);
                $response = $client->request('GET', env('ENDPOINT').'cotisations/generate/'.$inputs['membre'].'/date/'.$inputs['startDate'].'/'.$inputs['endDate'],[
                    'headers' => $this->verifHeaders(),
                ]);
            }
            
            $body = json_decode($response->getBody());
            //dd($body);
            return redirect()->back()->with('message', 'Opération en cour de traitement !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
