<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class DashboardController extends VerifController
{

    public function index($today="")
    {
        try {

            if (empty($today)) {
                $today = date('Y-m-d');
                $year  = Carbon::parse($today)->format('Y');
            } else {
                $year  = Carbon::parse($today)->format('Y');
            }

            session([
                'date' =>  $today, 
                'year' =>  $year, 
            ]);
            $client = new Client();

            $response = $client->request('GET', env('ENDPOINT').'categorie-messes',['headers' => $this->verifHeaders()]);
            $categories = json_decode($response->getBody()); 

            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders() ]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'users/all/statistiques/'.session('date'),['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            $statistique = $body->statistique ;

            //dd($statistique);

            $users  = $statistique->user ?? '0';
            $annees = $statistique->payeYear->montant ?? '0';
            $jours  = $statistique->payeByDay->montant ?? '0';
            $mois   = $statistique->payeByMonth->montant ?? '0';

            $moisMesse   = $statistique->payeByMonthMesse->montant ?? '0';
            $anneesMesse = $statistique->payeYearMesse->montant ?? '0';

            return view('pages.dashboard.index')->with([
                'categories'   => $categories,
                'groupes'      => $groupes,
                'statistiques' => [
                    'users'  => $users,
                    'annees' => $annees,
                    'mois'   => $mois 
                ],
                'statistiquesMesse' => [
                    'annees' => $anneesMesse,
                    'mois'   => $moisMesse 
                ]  
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function statistics(Request $request)
    {
        try {
            $inputs = $request->all();

            return $this->index($inputs['dates']);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function graph(Request $request, $groupId)
    {
        if ($request->ajax()) {
            try {
                $inputs = $request->all();
                $client = new Client();
                $response = $client->request('GET', env('ENDPOINT').'users/graph/statistiques/'.$groupId.'/'.session('year'),['headers' => $this->verifHeaders()]);
                $body = json_decode($response->getBody());

                return response()->json($body);

            } catch (ClientException $e) {
                return $this->verifErrors($e);
            } catch (RequestException $e) {
                return $this->verifErrorsServer($e);
            }
        }
    }


    public function graphMesse(Request $request, $categoryId)
    {
        if ($request->ajax()) {
            try {
                $inputs = $request->all();
                $client = new Client();
                $response = $client->request('GET', env('ENDPOINT').'users/graph/messe/statistiques/'.$categoryId.'/'.session('year'),['headers' => $this->verifHeaders()]);
                $body = json_decode($response->getBody());

                return response()->json($body);

            } catch (ClientException $e) {
                return $this->verifErrors($e);
            } catch (RequestException $e) {
                return $this->verifErrorsServer($e);
            }
        }
    }

}
