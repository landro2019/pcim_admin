<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\MembreRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class MembreController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders() ]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'users/0/100',['headers' => $this->verifHeaders() ]);
            $body = json_decode($response->getBody());

            return view('pages.membres.index')->with([
                'membres' => $body,
                'groupes' => $groupes,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function create(MembreRequest $request)
    {
        try {
            $inputs = $request->all();
            if (isset($inputs['groupe'])) {
                $groupe = ['id' => (int)($inputs['groupe'] )] ;
            } else {
                $groupe = null;
            }
            
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'users',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'firstname'  => $inputs['firstname'],
                    'lastname'   => $inputs['lastname'],
                    'phone'      => $inputs['phone'],
                    'profession' => $inputs['profession'],
                    'email'      => $inputs['email'],
                    'region'     => $inputs['region'],
                    'address'    => $inputs['address'],
                    'quartier'   => $inputs['quartier'],
                    'password'   => $inputs['password'] ?? '000000',
                    'sexe'       => $inputs['sexe'],
                    'group'      => $groupe
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Membre créé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id, $key) 
    {
        try {
            $client = new Client();

            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'users/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'user-cotisations/user/'.$id,['headers' => $this->verifHeaders()]);
            $cotisations = json_decode($response->getBody());

            if ($key == 'detail') {
                return view('pages.membres.detail')->with([
                    'membre'      => $body,
                    'cotisations' => $cotisations,
                ]);
            } elseif ($key == 'modifier') {
                return view('pages.membres.edit')->with([
                    'membre'      => $body,
                    'groupes'     => $groupes,
                ]);
            }
            
        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(MembreRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            if (isset($inputs['groupe'])) {
                $groupe = ['id' => (int)($inputs['groupe'] )] ;
            } else {
                $groupe = null;
            }
            $client = new Client();
            $response = $client->request('PATCH', env('ENDPOINT').'users/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'firstname'  => $inputs['firstname'],
                    'lastname'   => $inputs['lastname'],
                    'phone'      => $inputs['phone'],
                    'profession' => $inputs['profession'],
                    'email'      => $inputs['email'],
                    'region'     => $inputs['region'],
                    'address'    => $inputs['address'],
                    'quartier'   => $inputs['quartier'],
                    'sexe'       => $inputs['sexe'],
                    'group'      => $groupe
                ]
            ]);
            $body = json_decode($response->getBody());
            
            if ($body->statusCode == 200) {
                return to_route('membres')->with('message', 'Membre mise à jour avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de mise à jour !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function reset($id)
    {
        try {
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'users/reset/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Mot de passe reinitialisé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function paye($id, $key)
    {
        try {
            $client = new Client();
            if ($key == 'cash') {
                $response = $client->request('PUT', env('ENDPOINT').'user-cotisations/payment/espece/'.$id,['headers' => $this->verifHeaders()]);
                $body = json_decode($response->getBody());
            }
            
            if (strpos($body->message, 'Cotisation updated successfully') !== false) {
                return redirect()->back()->with('message', 'Paiement effectué avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de paiement, veuillez réessayer !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function payeM(Request $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'user-cotisations/payment',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'idCotisation' => $inputs['idCotisation'],
                    'phone'        => $inputs['phone'],
                ]
            ]);
            $body = json_decode($response->getBody());
            if (isset($body->data)) {
                return redirect()->back()->with('payment', $body->data);
            } else {
                return redirect()->back()->with('error', 'Echec de l\'opération !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function paye_status($id) {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'user-cotisations/payment/status/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            if (isset($body->data)) {
                return redirect()->back()->with('message', 'Votre opération est en cour de traitement !');
            } else {
                return redirect()->back()->with('error', 'Echec de l\'opération !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function role($id) {
        try {
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'users/change/role/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            //dd($body);
            
            if ($body->statusCode == 200) {
                return redirect()->back()->with('message', 'Niveau d\'accès mise à jour avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }

}
