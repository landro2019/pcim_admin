<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GroupeController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return view('pages.groupes.index')->with([
                'groupes' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
    

    

    public function create(GroupRequest $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'groups',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'label'       => $inputs['label'],
                    'description' => $inputs['description'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Groupe créé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'groups/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Groupe supprimé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'groups/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return view('pages.groupes.edit')->with([
                'groupe' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(GroupRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('PATCH', env('ENDPOINT').'groups/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'label'       => $inputs['label'],
                    'description' => $inputs['description'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return to_route('groupes')->with('message', 'Groupe mise à jour avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
