<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use App\Http\Requests\NotificationRequest;
use GuzzleHttp\Exception\RequestException;

class NotificationController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'notifications',['headers' => $this->verifHeaders()]);
            $notifications = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'users/0/100',['headers' => $this->verifHeaders()]);
            $membres = json_decode($response->getBody());
            
            return view('pages.notifications.index')->with([
                'notifications' => $notifications,
                'groupes'       => $groupes,
                'membres'       => $membres,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
    

    

    public function create(NotificationRequest $request)
    {
        try {
            $inputs = $request->all();
            //dd($inputs);
            $client = new Client();
            if (!isset($inputs['membre']) && !isset($inputs['groupe'])) {
                return redirect()->back()->with('error', 'Veuillez selectionner soit un groupe ou un membre !');
            }
            $response = $client->request('POST', env('ENDPOINT').'notifications',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'libelle'        => $inputs['libelle'],
                    'description'    => $inputs['description'],
                    'user_id'        => $inputs['membre'] ?? null, // ['id' => (int)($inputs['membre'] ?? null)],
                    'group_id'       => $inputs['groupe'] ?? null, // ['id' => (int)($inputs['groupe'] ?? null)],
                    'date_lancement' => Carbon::parse($inputs['date_lancement'])->format('d-m-Y'),
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Notification créée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'notifications/'.$id,[
                'headers' => $this->verifHeaders(),
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Notification supprimée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();

            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'users/0/100',['headers' => $this->verifHeaders()]);
            $membres = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'notifications/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            if (isset($body) && $body->data !== null && $body->data !== '') {
                $body->data->date_lancement = date_format(new DateTime($body->data->date_lancement), 'Y-m-d');
            }

            return view('pages.notifications.edit')->with([
                'notification' => $body->data,
                'groupes'       => $groupes,
                'membres'       => $membres,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(NotificationRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            if (isset($inputs['membre']) && isset($inputs['groupe'])) {
                return redirect()->back()->with('error', 'Veuillez selectionner soit un groupe ou un membre !');
            }
            $response = $client->request('PUT', env('ENDPOINT').'notifications/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'libelle'        => $inputs['libelle'],
                    'description'    => $inputs['description'],
                    'user_id'        => $inputs['membre'] ?? null, // ['id' => (int)($inputs['membre'] ?? null)],
                    'group_id'       => $inputs['groupe'] ?? null, // ['id' => (int)($inputs['groupe'] ?? null)],
                    'date_lancement' => Carbon::parse($inputs['date_lancement'])->format('d-m-Y'),
                ]
            ]);
            $body = json_decode($response->getBody());

            if ($body->statusCode == 200) {
                return to_route('notifications')->with('message', 'Notification mise à jour avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de mise à jour !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
