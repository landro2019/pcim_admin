<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategorieMesseRequest;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class CategorieMesseController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'categorie-messes',['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return view('pages.messes.categorie-messes.index')->with([
                'categories' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function create(CategorieMesseRequest $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'categorie-messes',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'nom'        => $inputs['nom'],
                    'montant'    => $inputs['montant'],
                    'nombreJour' => $inputs['nombreJour'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Catégorie de messe créée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'categorie-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Catégorie de messe supprimée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'categorie-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return view('pages.messes.categorie-messes.edit')->with([
                'categorie' => $body,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(CategorieMesseRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'categorie-messes/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'nom'        => $inputs['nom'],
                    'montant'    => $inputs['montant'],
                    'nombreJour' => $inputs['nombreJour'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return to_route('messes.categories')->with('message', 'Catégorie de messe mise à jour avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
