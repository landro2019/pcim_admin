<?php

namespace App\Http\Controllers;

use App\Http\Requests\DefautMesseRequest;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class DefautMesseController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'categorie-messes',['headers' => $this->verifHeaders()]);
            $categories = json_decode($response->getBody()); 

            $response = $client->request('GET', env('ENDPOINT').'defaut-messes',['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            return view('pages.messes.messe-defaut.index')->with([
                'defauts'    => $body,
                'categories' => $categories,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function create(DefautMesseRequest $request)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'defaut-messes',[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'categorie'  => ['id' => (int)($inputs['categorieId'])] ,
                    'message'    => $inputs['message'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Catégorie de messe créée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'defaut-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Catégorie de messe supprimée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'categorie-messes',['headers' => $this->verifHeaders()]);
            $categories = json_decode($response->getBody()); 

            $response = $client->request('GET', env('ENDPOINT').'defaut-messes/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            return view('pages.messes.messe-defaut.edit')->with([
                'defaut'     => $body,
                'categories' => $categories,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(DefautMesseRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'defaut-messes/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'categorie'  => ['id' => (int)($inputs['categorieId'])] ,
                    'message'    => $inputs['message'],
                ]
            ]);
            $body = json_decode($response->getBody());
            return to_route('messes.defauts')->with('message', 'Catégorie de messe mise à jour avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
