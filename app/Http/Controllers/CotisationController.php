<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\CotisationRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class CotisationController extends VerifController
{
    public function index()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'cotisations',['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            //dd($body);

            return view('pages.cotisations.index')->with([
                'cotisations' => $body,
                'groupes'     => $groupes,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function create(CotisationRequest $request)
    {
        try {
            $inputs = $request->all();
            $values = [
                'type'        => $inputs['type'],
                'montant'     => $inputs['montant'],
                'date_start'  => Carbon::parse($inputs['date_start'])->format('d-m-Y'),
                'description' => $inputs['description'],
                'etat'        => $inputs['etat'],
                'group'       => ['id' => (int)($inputs['groupe'])] 
            ];
            //dd( $values );
            $values = [
                'type'        => $inputs['type'],
                'montant'     => $inputs['montant'],
                'date_start'  => Carbon::parse($inputs['date_start'])->format('d-m-Y'),
                'description' => $inputs['description'],
                'etat'        => $inputs['etat'],
                'group'       => ['id' => (int)($inputs['groupe'])] 
            ];
            $client = new Client();
            $response = $client->request('POST', env('ENDPOINT').'cotisations',[
                'headers' => $this->verifHeaders(),
                'json'    => $values
            ]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Cotisation créée avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function get_byId($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('GET', env('ENDPOINT').'groups',['headers' => $this->verifHeaders()]);
            $groupes = json_decode($response->getBody());

            $response = $client->request('GET', env('ENDPOINT').'cotisations/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());

            if (isset($body) && $body->data !== null && $body->data !== '') {
                $body->data->date_start = date_format(new DateTime($body->data->date_start), 'Y-m-d');
            }
            return view('pages.cotisations.edit')->with([
                'cotisation' => $body->data,
                'groupes'    => $groupes,
            ]);

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function update(CotisationRequest $request, $id)
    {
        try {
            $inputs = $request->all();
            if (!isset($inputs['type'])) {
                return redirect()->back()->with('error', 'Veuillez choisir un type de cotisation !');
            }
            $client = new Client();
            $response = $client->request('PUT', env('ENDPOINT').'cotisations/'.$id,[
                'headers' => $this->verifHeaders(),
                'json'    => [
                    'type'        => $inputs['type'],
                    'montant'     => $inputs['montant'],
                    'date_start'  => Carbon::parse($inputs['date_start'])->format('d-m-Y'),
                    'description' => $inputs['description'],
                    'etat'        => $inputs['etat'],
                    'group'       => $inputs['groupe'],
                ]
            ]);
            $body = json_decode($response->getBody());

            if ($body->statusCode == 200) {
                return to_route('cotisations')->with('message', 'Cotisation mise à jour avec succès !');
            } else {
                return redirect()->back()->with('error', 'Echec de mise à jour !');
            }

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function delete($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'cotisations/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Cotisation supprimé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }


    public function deleteUser($id) 
    {
        try {
            $client = new Client();
            $response = $client->request('delete', env('ENDPOINT').'user-cotisations/'.$id,['headers' => $this->verifHeaders()]);
            $body = json_decode($response->getBody());
            return redirect()->back()->with('message', 'Cotisation du membre supprimé avec succès !');

        } catch (ClientException $e) {
            return $this->verifErrors($e);
        } catch (RequestException $e) {
            return $this->verifErrorsServer($e);
        }
    }
}
