<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VerifController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!session('user')) {
                session()->flush();
                return redirect()->route('login.view')->with('error', 'Veuillez-vous reconnecter !');
            }
            return $next($request);
        });
    }

    public function verifHeaders()
    {
        return [
            'Accept'        => 'application/json',
            'X-api-key'     => env('API_KEY'),
            'Authorization' => 'Bearer '.session('token')
        ];
    }

    public function verifErrors($e) {
        $body = $e->getResponse()->getBody()->getContents();
        $decodedBody = json_decode($body, true); 
        $messageErreurDecode = $decodedBody['message'] ?? $decodedBody['error']; 

        if (isset($messageErreurDecode['message']) && is_array($messageErreurDecode['message']) /* && count($messageErreurDecode['message']) > 1 */) {
            return redirect()->back()->with('erreurs', $messageErreurDecode['message']);
        } elseif (strpos($messageErreurDecode, 'Unauthorized') !== false) {
            session()->flush();
            return redirect()->route('login.view')->with('error', 'Veuillez-vous connecter !');
        } elseif (strpos($messageErreurDecode, 'User already') !== false) {
            return redirect()->back()->with('error', 'Cet membre existe déjà !');
        } else {
            return redirect()->back()->with('error', $messageErreurDecode);
        }
    }

    public function verifErrorsServer($e) {
        if ($e->getResponse() == null) {
            return redirect()->back()->with('error', 'Quelque chose s\'est mal passée !');
        }
        $body = $e->getResponse()->getBody()->getContents();

        $decodedBody = json_decode($body, true); 
        $messageErreurDecode = $decodedBody['message'] ?? $decodedBody['error']; 

        if (strpos($messageErreurDecode, 'Internal server error') !== false) {
            return redirect()->back()->with('error', 'Erreur de requête : Erreur interne du serveur');
        } else {
            return redirect()->back()->with('error', 'Erreur de requête : ' . $messageErreurDecode);
        }
        
    }
}
