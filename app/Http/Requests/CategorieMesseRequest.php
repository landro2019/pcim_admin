<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategorieMesseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nom'        => 'required',
            'montant'    => 'required',
            'nombreJour' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nom.required'        => 'Un nom est réquis !',
            'montant.required'    => 'Un montant est réquis !',
            'nombreJour.required' => 'Un nombre de jour est réquis !',
        ];
    }
}
