<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DefautMesseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'categorieId' => 'required',
            'message'     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'categorieId.required' => 'Une categorie de messe est réquis !',
            'message.required'     => 'Un message est réquis !',
        ];
    }
}
