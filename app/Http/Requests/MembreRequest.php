<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MembreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'firstname'  => 'required',
            'lastname'   => 'required',
            'phone'      => 'required|numeric',
            'profession' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'firstname.required'  => 'Un nom est réquis !',
            'lastname.required'   => 'Un prénom est réquis !',
            'phone.required'      => 'Un numéro de téléphone est réquis !',
            'phone.numeric'       => 'Numéro de téléphone est invalide !',
            'profession.required' => 'Une profession est réquise !',
        ];
    }
}
