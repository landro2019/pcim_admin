<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnonceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'titre'      => 'required',
            'auteur'     => 'required',
            'message'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'titre.required'      => 'Un titre est réquis !',
            'auteur.required'     => 'Un auteur est réquis !',
            'message.required'    => 'Un message est réquis !'
        ];
    }
}
