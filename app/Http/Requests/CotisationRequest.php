<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CotisationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'groupe'      => 'required',
            'type'        => 'required',
            'montant'     => 'required|numeric',
            'date_start'  => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'groupe.required'      => 'Un groupe est réquis !',
            'type.required'        => 'Un type de cotiation est réquis !',
            'montant.required'     => 'Un montant de cotisation est réquis !',
            'montant.numeric'      => 'Montant de cotisation invalide !',
            'date_start.required'  => 'Une date de lancement est réquise !',
            'description.required' => 'Une description est réquise !',
        ];
    }
}
