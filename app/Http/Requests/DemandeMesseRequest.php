<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DemandeMesseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user'      => 'required',
            'categorie' => 'required',
            'message'   => 'required',
            'dateDebut' => 'required',
            //'dateFin'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user.required'      => 'Un membre est réquis !',
            'categorie.required' => 'Une categorie est réquis !',
            'message.required'   => 'Un message est réquis !',
            'dateDebut.required' => 'Une date de début est réquis !',
            //'dateFin.required'   => 'Une date de fin est réquis !',
        ];
    }
}
